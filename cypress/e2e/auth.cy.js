import { faker } from "@faker-js/faker";

const name = faker.person.fullName(),
  email = faker.internet.email(),
  password = "testPassword";

describe("Authentication", () => {
  it("should open the sign in modal", () => {
    cy.visit("/");
    cy.get('a[href*="signin"]').click();
    cy.url().should("include", "/signin");
    cy.get("h2").contains("Sign In");
    cy.get(".modal").should("exist");
  });

  it("Should open the sign up modal and sign up", () => {
    cy.visit("/auth/signup");
    cy.get("h2").contains("Sign Up");

    cy.get("#name").type(name);
    cy.get("#name").should("have.value", name);

    cy.get("#email").type(email);
    cy.get("#email").should("have.value", email);

    cy.get("#password").type(password);
    cy.get("#password").should("have.value", password);

    cy.get("#passwordConfirmation").type(password);
    cy.get("#passwordConfirmation").should("have.value", password);

    cy.get("[type=submit]").click();

    cy.wait(1000);
  });

  it("Should sign out", () => {
    cy.login(email, password);
    cy.visit("/");
    cy.get("button").contains("Sign Out").click();
    cy.wait(100);
    cy.getCookie("next-auth.session-token").should("not.exist");
  });
});
