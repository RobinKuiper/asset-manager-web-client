describe("Mobile", () => {
  beforeEach(() => {
    cy.viewport(550, 750);

    cy.visit("/");

    cy.get(".sidebar.left").as("leftSidebar");
    cy.get(".handler.left").as("leftHandler");
    cy.get(".sidebar.right").as("rightSidebar");
    cy.get(".handler.right").as("rightHandler");
  });

  it("show and hide left sidebar", () => {
    cy.get("@leftSidebar").should("not.have.class", "active");
    cy.get("@leftHandler").should("not.have.class", "active");

    cy.get("@rightSidebar").should("not.have.class", "active");
    cy.get("@rightHandler").should("not.have.class", "active");

    cy.get("@leftHandler").click();

    cy.get("@leftSidebar").should("have.class", "active");
    cy.get("@leftHandler").should("have.class", "active");
    cy.get("@rightHandler").should("have.css", "opacity", "0");

    cy.get("@leftHandler").click();

    cy.get("@leftSidebar").should("not.have.class", "active");
    cy.get("@leftHandler").should("not.have.class", "active");
    cy.get("@rightHandler").should("have.css", "opacity", "1");
  });

  it("show and hide right sidebar", () => {
    cy.get("@leftSidebar").should("not.have.class", "active");
    cy.get("@leftHandler").should("not.have.class", "active");

    cy.get("@rightSidebar").should("not.have.class", "active");
    cy.get("@rightHandler").should("not.have.class", "active");

    cy.get("@rightHandler").click();

    cy.get("@rightSidebar").should("have.class", "active");
    cy.get("@rightHandler").should("have.class", "active");
    cy.get("@leftHandler").should("have.css", "opacity", "0");

    cy.get("@rightHandler").click();

    cy.get("@rightSidebar").should("not.have.class", "active");
    cy.get("@rightHandler").should("not.have.class", "active");
    cy.get("@leftHandler").should("have.css", "opacity", "1");
  });

  it("show and hide the topbar menu", () => {
    cy.get(".topbar nav").as("topbarNav");
    cy.get(".topbar .right").as("right");
    cy.get(".topbar .hamburger").as("hamburger");

    cy.get("@topbarNav").should("have.css", "opacity", "0");
    cy.get("@right").should("have.css", "opacity", "0");
    cy.get("@hamburger").should("have.css", "display", "block");

    cy.get("@hamburger").click();

    cy.get("@hamburger").should("have.class", "open");
    cy.get("@topbarNav").should("have.css", "opacity", "1");
    cy.get("@right").should("have.css", "opacity", "1");
  });
});
