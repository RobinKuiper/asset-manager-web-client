import { faker } from "@faker-js/faker";

describe("Authentication", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.fixture("example.json").then(fixtures => {
      cy.login(fixtures.adminEmail, fixtures.adminPassword);
    });
  });

  it("should login and go to the admin page", () => {
    cy.visit("/admin");
    cy.get("h1").contains("Admin");
  });

  it("should display permissions page", () => {
    cy.visit("/admin/permissions");
    cy.get("h2").contains("Permissions");
    cy.get("td").contains("user.get.all");
    cy.get(".pagination:nth-child(3) .next").click();
    cy.get("td").contains("log.get.all");
    cy.get(".search-container input").type("ThisWillFindNothing");
    cy.get("td").contains("No data found.");
  });

  it("should create a permission", () => {
    cy.visit("/admin/permissions");
    cy.get("[title='Create permission'").click();
    cy.url().should("include", "/permissions/create");
    cy.get("h2").contains("Create Permission");
    cy.get(".modal").should("exist");

    cy.get("#name").type("new.permission");
    cy.get("#name").should("have.value", "new.permission");

    cy.get("#description").type("This is a new permission");
    cy.get("#description").should("have.value", "This is a new permission");

    cy.get("[type=submit]").click();

    cy.visit("/admin/permissions");
    cy.get(".pagination:nth-child(3) .next").click();
    cy.get("td").contains("new.permission");
  });

  it("should show errors on faulty data", () => {
    cy.visit("/admin/permissions");
    cy.get("[title='Create permission'").click();
    cy.url().should("include", "/permissions/create");
    cy.get("h2").contains("Create Permission");
    cy.get(".modal").should("exist");
    cy.get("#name").type("2");
    cy.get("#description").type("2");
    cy.get("[type=submit]").click();
    cy.get(".error").contains("Name must be at least 3 characters long");
    cy.get(".error").contains("Description must be at least 5 characters long");
  });
});
