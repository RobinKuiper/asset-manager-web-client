describe("Navigation", () => {
  it("should navigate to the about page", () => {
    // Start from the index page
    cy.visit("http://localhost:3000/");

    // Find a link with an href attribute containing "longpage" and click it
    cy.get('a[href*="longpage"]').click();

    // The new url should include "/longpage"
    cy.url().should("include", "/longpage");

    // The new page should contain an h1 with "Long text"
    cy.get("h1").contains("Long text");
  });
});
