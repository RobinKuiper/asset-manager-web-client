/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

import type { Config } from "jest";
import nextJest from "next/jest";

const createJestConfig = nextJest({
  // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
  dir: "./",
});

const config: Config = {
  // Automatically clear mock calls, instances, contexts and results before every test
  clearMocks: true,
  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,
  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",
  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "v8",
  coverageReporters: ["clover", "json", "text", "lcov", "cobertura"],
  collectCoverageFrom: ["**/*.{js,jsx,ts,tsx}", "!**/node_modules/**", "!**/*.config.**", "!**/*.d.**"],
  // transformIgnorePatterns: [`/node_modules/(?!${esModules})`],
  // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
  moduleNameMapper: {
    "^jose": require.resolve("jose"),
    "^@panva/hkdf": require.resolve("@panva/hkdf"),
    "^uuid": require.resolve("uuid"),
    "^preact-render-to-string": require.resolve("preact-render-to-string"),
    "^preact": require.resolve("preact"),
    "^@/components/(.*)$": "<rootDir>/components/$1",
  },
  testMatch: ["**/?(*.)+(test).[jt]s?(x)"],
  // A list of paths to directories that Jest should use to search for files in
  // roots: ["<rootDir>/__tests__/unit"],
  // A list of paths to modules that run some code to configure or set up the testing framework before each test
  setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
  // The test environment that will be used for testing
  testEnvironment: "jsdom",
};

export default createJestConfig(config);
