<div align="center">

[![made with coffee by Robin Kuiper](https://img.shields.io/badge/made%20with%20☕%20by-Robin%20Kuiper-ff1414.svg?style=flat-square)](https://gitlab.com/RobinKuiper)
[![Website](https://img.shields.io/badge/Visit-Website-blue?logo=google-chrome)](https://www.rkuiper.nl)

[![Latest Release](https://gitlab.com/RobinKuiper/boilerplate-next-client/-/badges/release.svg)](https://gitlab.com/RobinKuiper/boilerplate-next-client/-/releases)
[![pipeline status](https://gitlab.com/RobinKuiper/boilerplate-next-client/badges/develop/pipeline.svg)](https://gitlab.com/RobinKuiper/boilerplate-next-client/-/commits/develop)

[![Logo](src/images/logo.webp)](https://github.com/RobinKuiper/project-boilerplate)

# Boilerplate for Next.js 14+, SASS, and TypeScript.

🚀 Boilerplate and Starter for Next.js with App Router support, SASS, and TypeScript ⚡️ Prioritizing developer experience first: Next.js, TypeScript, ESLint, Prettier, Husky, Lint-Staged, Jest, Cypress, Commitlint, SASS, Authentication with NextAuth, Logging with Winston, and Multi-language (i18n).

Used with an external [api](https://gitlab.com/RobinKuiper/boilerplate-slim-api)

</div>

<details>
<summary>Screenshots</summary>
<br>

|                              Home Page                              |                             Sign In                             |                            Admin                             |
| :-----------------------------------------------------------------: | :-------------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="docs/images/homepage.png" title="Home Page" width="100%"> | <img src="docs/images/signin.png" title="Sign In" width="100%"> | <img src="docs/images/admin.png" title="Admin" width="100%"> |

</details>

---

<details open="open">
<summary>Table of Contents</summary>

- [About](#about)
  - [Requirements](#requirements)
  - [Project Structure](#project-structure)
- [Installation](#installation)
  - [Environment](#environment)
  - [API](#api)
- [Usage](#usage)
  - [Commit Message Format](#commits)
  - [Logging](#logging)
  - [Unit Testing](#unit-testing)
  - [E2E Testing](#e2e-testing)
  - [Translation](#translations)
  - [Customization](#customization)
- [Deployment](#deployment)
- [Environment Variables](#env-vars)
- [PNPM Commands](#pnpm-commands)

</details>

<a id="about"></a>

## About

<a id="requirements"></a>

### Requirements

- Node.js 18+ and pnpm

<a id="project-structure"></a>

### Project structure

```shell
.
├── README.md                       # README file
├── .github                         # GitHub folder
├── .husky                          # Husky configuration
├── .idea                           # Intellij configuration
├── public                          # Public assets folder
├── scripts                         # Scripts folder
├── src
│   ├── action                      # Server actions
│   ├── app                         # Next JS App (App Router)
│   ├── components                  # React components
│   ├── images                      # Images folder
│   ├── libs?                       # 3rd party libraries configuration
│   ├── locales                     # Locales folder (i18n messages)
│   ├── styles                      # Styles folder
│   ├── templates?                  # Templates folder
│   ├── types                       # Type definitions
│   ├── utils                       # Utilities folder
│   └── validations?                # Validation schemas
├── tests
│   ├── cypress                     # Cypress setup
│   │   ├── e2e                     # E2E tests
│   └── unit                        # Unit tests
├── .editorconfig                   # Intellij editor configuration
├── .env*                           # Environment configuration files
├── .eslintrc.json                  # Eslint configuration
├── .lintstagedrc.js                # lint-staged configuration
├── .prettierrc                     # Prettier configuration
├── cypress.config.ts               # Cypress configuration
├── jest.config.ts                  # Jest configuration
├── next.config.mjs                 # NextJS configuration
└── tsconfig.json                   # TypeScript configuration
```

<a id="installation"></a>

## Installation

Run the following command on your local environment:

```shell
git clone https://gitlab.com/RobinKuiper/boilerplate-next-client.git my-project-name
cd my-project-name
pnpm install
```

Set up the [environment](#environment) variables you need, [see below](#environment).

You can run the following command to enable Git hooks:

```shell
pnpm prepare
```

Then, you can run the project locally in development mode with live reload by executing:

```shell
pnpm dev
```

Open http://localhost:3000 with your favorite browser to see your project.

<a id="environment"></a>

### Environment

There are multiple `.env` files to configure the environment:

- **.env** - Global environment variables, used in all environments
- **.env.development** - Environment variables used in the development environment, overwrites global variables.
- **.env.production** - Environment variables used in the production environment, overwrites global variables.
- **.env.test** - Environment variables used in the test environment, overwrites global variables.
- **.env.local** - Environment variables used in you local environment, overwrites all others.

You can set up a new project by copying the `.env.example` to `.env.local` and fill in the variables you need.

See [Environment variables](#env-vars) for more information about the variables

<a id="api"></a>

### Set up the api

Go to [Boilerplate Slim Api](https://gitlab.com/RobinKuiper/boilerplate-slim-api) and follow the instructions.

<a id="usage"></a>

## Usage

<a id="commits"></a>

### Commit Message Format

The project enforces [Conventional Commits](https://www.conventionalcommits.org/) specification. This means that all your commit messages must be formatted according to the specification.

One of the benefits of using Conventional Commits is that it allows us to automatically generate a `CHANGELOG` file. It also allows us to automatically determine the next version number based on the types of commits that are included in a release.

<a id="logging"></a>

### Logging

The project uses Winston for logging. The logs are displayed in the console and written to the `./logs' directory by default.

<a id="unit-testing"></a>

### Unit Testing

All unit tests are located with the source code inside the same directory. So, it makes it easier to find them. The project uses Jest and React Testing Library for unit testing. You can run the tests with:

```shell
pnpm test:unit
pnpm test:unit:watch
```

<a id="e2e-testing"></a>

### E2E Testing

The project uses Cypress for E2E testing. You can run the tests interactively with:

```shell
pnpm test:e2e
```

Or headless (eg. for CI):

```shell
pnpm test:e2e:headless
```

Starting these tests will concurrently start a development environment (`pnpm dev`).

<a id="translations"></a>

### Translation (i18n) setup

For translation, the project uses `next-intl`. Configuring locale settings can be done in `./src/AppConfig.ts`.

You can find the locale files in `./src/locales`.

<a id="customization"></a>

### Customization

You can easily configure NextJs Boilerplate by making a search in the whole project with `FIXME:` for making quick customization. Here is some of the most important files to customize:

- `public/apple-touch-icon.png`, `public/favicon.ico`, `public/favicon-16x16.png` and `public/favicon-32x32.png`: your website favicon, you can generate from https://favicon.io/favicon-converter/
- `next.config.mjs`: Next.js configuration
- `src/AppConfig.ts`: configuration file

<a id="deployment"></a>

### Deploy to production

You can generate a production build with:

```shell
$ pnpm build
```

It generates an optimized production build of the boilerplate. For testing the generated build, you can run:

```shell
$ pnpm start
```

Make sure that you have created a `.env.local` with the correct variables, will overwrite `.env` and `.env.production`.

The command starts a local server with the production build. Then, you can now open http://localhost:3000 with your favorite browser to see the project.

#### Standalone

The build command also generates a standalone build.

After completing a build there will be a folder in `.next` called `standalone`, in here you can run the following command to start the build with **Node**:

```bash
node server.js
```

Standalone build mode is enabled in the `next.config.mjs` file by default.

<a id="env-vars"></a>

## Environment Variables

|    Variable     |                        Description                         |
| :-------------: | :--------------------------------------------------------: |
|  NEXTAUTH_URL   |              Url where this project will run               |
| NEXTAUTH_SECRET | A secret used by NextAuth (must be at least 32 characters) |
|     API_URL     |                   Url of the api server                    |
|   API_VERSION   |             Version of the api to use, eg. v1              |

<a id="pnpm-commands"></a>

## PNPM Commands

|        Command         |                                  Description                                   |
| :--------------------: | :----------------------------------------------------------------------------: |
|        pnpm dev        |                         Starts the development server                          |
|       pnpm build       |                     Builds the application for production                      |
|    pnpm build:stats    |                        Show build stats in the browser                         |
|       pnpm start       |                        Starts a previous build project                         |
|       pnpm lint        |                    Lints the code and shows linting errors                     |
|      pnpm format       | Formats (if possible) the projects code to the lint and prettier configuration |
|    pnpm check:types    |                  Checks typescript types in the project code                   |
|      pnpm cypress      |                             Opens the Cypress app                              |
|     pnpm test:unit     |                              Runs Jest unit tests                              |
|  pnpm test:unit:watch  |               Runs Jest unit tests and watches for code changes                |
|     pnpm test:e2e      |                       Opens the Cypress app on E2E tests                       |
| pnpm test:e2e:headless |                      Runs the Cypress E2E tests headless                       |
|       pnpm clean       |                               Cleans the project                               |
|      pnpm prepare      |                            Enables Husky git hooks                             |

#### Bundle Analyzer

Next.js Boilerplate comes with a built-in bundle analyzer. It can be used to analyze the size of your JavaScript bundles. To begin, run the following command:

```shell
pnpm build:stats
```

By running the command, it'll automatically open a new browser window with the results.

---

Made with ☕ by [Robin Kuiper](https://gitlab.com/RobinKuiper)
