export const getFieldValue = (object: { [key: string]: { [key: string]: unknown } | unknown }, fieldPath: string) => {
  const keys = fieldPath.split(".");
  let temp = object;
  let value;

  // Loop through object until we find the last child
  for (const key of keys) {
    if (temp && key in temp) {
      if (typeof temp[key] === "object") {
        temp = temp[key] as { [key: string]: { [key: string]: unknown } };
      } else {
        value = temp[key] as unknown;
      }
    } else {
      return undefined;
    }
  }

  return value;
};
