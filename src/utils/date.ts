export function formatDateWithLocale(timestamp: number) {
  if (timestamp <= 0) {
    throw new Error("This is not a valid timestamp");
  }

  const tStamp = isMilliSeconds(timestamp) ? timestamp : timestamp * 1000;
  const date = new Date(tStamp);
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    hour12: false,
  };

  // Format date based on locale
  const formatter = new Intl.DateTimeFormat([], options);
  return formatter.format(date);
}

export function isMilliSeconds(timestamp: number) {
  // Return true if the timestamp is in milliseconds
  return timestamp <= 1000 * Math.floor(Math.abs(timestamp / 1000)); // Consider negative values
}
