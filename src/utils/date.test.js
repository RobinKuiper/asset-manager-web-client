import { formatDateWithLocale, isMilliSeconds } from "./date";

const inSeconds = 1516239022,
  inMilliSeconds = 1516239022000;

describe("formatDateWithLocale", () => {
  it("formats date in milliseconds correctly", () => {
    const formattedDate = formatDateWithLocale(inMilliSeconds);
    expect(formattedDate).toContain("January 18, 2018 at");
  });

  it("formats date in seconds correctly", () => {
    const formattedDate = formatDateWithLocale(inSeconds);
    expect(formattedDate).toContain("January 18, 2018 at");
  });

  it("handles timestamps outside expected range", () => {
    expect(() => formatDateWithLocale(Number.MAX_SAFE_INTEGER)).toThrow();
    expect(() => formatDateWithLocale(-1000)).toThrow();
  });
});

describe("isMilliSeconds", () => {
  it("correctly identifies milliseconds", () => {
    expect(isMilliSeconds(inMilliSeconds)).toBe(true);
  });

  it("correctly identifies seconds", () => {
    expect(isMilliSeconds(inSeconds)).toBe(false);
  });
});
