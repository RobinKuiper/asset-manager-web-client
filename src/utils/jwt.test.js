import { decodeJwtToken } from "@/utils/jwt";

describe("decodeJwtToken", () => {
  it("decodes a valid JWT token correctly", () => {
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"; // Example token
    const decodedData = decodeJwtToken(token);
    expect(decodedData).toEqual({
      sub: "1234567890",
      name: "John Doe",
      iat: 1516239022,
    });
  });

  it("throws an error for invalid JWT format", () => {
    const invalidToken = "invalid.token.format";
    expect(() => decodeJwtToken(invalidToken)).toThrow();
  });

  it("throws an error for invalid base64-encoded payload", () => {
    const tokenWithInvalidPayload = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.invalidPayload.signature";
    expect(() => decodeJwtToken(tokenWithInvalidPayload)).toThrow();
  });

  it("throws an error for invalid JSON payload", () => {
    const tokenWithInvalidJson = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.invalidJson.signature";
    expect(() => decodeJwtToken(tokenWithInvalidJson)).toThrow();
  });
});
