import { getFieldValue } from "@/utils/objects";

describe("getFieldValue", () => {
  it("gets a value from a nested object using a valid field path", () => {
    const data = {
      user: {
        name: "John Doe",
        address: {
          city: "New York",
          country: "USA",
        },
      },
    };
    const fieldName = "user.address.city";
    const value = getFieldValue(data, fieldName);
    expect(value).toBe("New York");
  });

  it("returns undefined for a non-existent field path", () => {
    const data = {
      user: {
        name: "John Doe",
      },
    };
    const fieldName = "user.address.zip";
    const value = getFieldValue(data, fieldName);
    expect(value).toBeUndefined();
  });

  it("returns undefined for an empty object", () => {
    const data = {};
    const fieldName = "user.name";
    const value = getFieldValue(data, fieldName);
    expect(value).toBeUndefined();
  });

  it("handles cases where intermediate objects are not objects", () => {
    const data = {
      user: "John Doe", // user is a string here, not an object
      address: {
        city: "New York",
      },
    };
    const fieldName = "user.address.city";
    const value = getFieldValue(data, fieldName);
    expect(value).toBe("New York"); // Still retrieves value from valid path
  });

  it("returns undefined if final key points to a non-existent property", () => {
    const data = {
      user: {
        name: "John Doe",
        address: {
          city: "New York",
        },
      },
    };
    const fieldName = "user.address.state"; // state property doesn't exist
    const value = getFieldValue(data, fieldName);
    expect(value).toBeUndefined();
  });
});
