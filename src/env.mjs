import { z } from "zod";

const Env = z.object({
  NODE_ENV: z
    .literal("development")
    .or(z.literal("production").or(z.literal("test")))
    .optional(),
  NEXTAUTH_SECRET: z.string().min(32),
  NEXTAUTH_URL: z.string().url(),
  API_URL: z.string().url(),
  API_VERSION: z.string().regex(/v(\d)+/, {
    message: "API_VERSION should be in the format v#, eg. v1, v2, etc.",
  }),
});

const config = Env.parse(process.env);

export default config;
