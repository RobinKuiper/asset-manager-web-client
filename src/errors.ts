export const errors: { [key: string]: string } = {
  CredentialsSignin: "Invalid email or password.",
};
