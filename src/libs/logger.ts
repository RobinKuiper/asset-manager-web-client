import winston, { format as winstonFormat } from "winston";
import DailyRotateFile, { DailyRotateFileTransportOptions } from "winston-daily-rotate-file";
import prettyPrint = winstonFormat.prettyPrint;
import env from "@/env.mjs";
const { combine, timestamp, json } = winston.format;

const format = combine(timestamp(), json());

const createFileTransport = (fileName: string, level?: string) => {
  const options: DailyRotateFileTransportOptions = {
    filename: `logs/${fileName}.log`,
  };

  if (level) {
    options.level = level;
  }

  return new DailyRotateFile(options);
};

const logger = winston.createLogger({
  level: "info",
  format,
  transports: [createFileTransport("error", "error"), createFileTransport("combined")],
});

// If we're not in production then also log to the console
if (env.NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      format: combine(prettyPrint(), winston.format.colorize({ all: true })),
    }),
  );

  logger.add(createFileTransport("debug", "debug"));
}

export { logger };
