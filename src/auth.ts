import { getServerSession, NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { GetServerSidePropsContext, NextApiRequest, NextApiResponse } from "next";
import apiFetch from "@/api";
import { decodeJwtToken } from "@/utils/jwt";
import { AuthResponse } from "@/types/response";
import { logger } from "@/libs/logger";
import env from "@/env.mjs";

const providers = [
  CredentialsProvider({
    type: "credentials",
    id: "credentials",
    name: "Credentials",
    credentials: {
      email: { label: "email", type: "email", placeholder: "E-mail address", value: "rob@gmail.com" },
      password: { label: "Password", type: "password", placeholder: "Password", value: "Ro1988bin" },
    },
    authorize: async credentials => {
      const body = JSON.stringify(credentials);

      const response: AuthResponse = await apiFetch({ path: "/auth/login", method: "POST", body });

      if (response.errors || !response.data.accessToken) {
        logger.error("Credentials - Authorize", {
          errors: response.errors,
        });
        return null;
      }

      const token = decodeJwtToken(response.data.accessToken);
      token.accessToken = response.data.accessToken;
      token.refreshToken = response.data.refreshToken;

      logger.info("Credentials - Authenticated", {
        email: token.email,
      });

      return token;
    },
  }),
  // GoogleProvider({
  //     clientId: "3770459521-ufu9dupuqidofjhsms30emp3sjlhdkv8.apps.googleusercontent.com",
  //     clientSecret: "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",
  //     authorization: {
  //         params: {
  //             prompt: "consent",
  //             access_type: "offline",
  //             response_type: "code"
  //         }
  //     }
  // })
];

const pages = {
  signIn: "/auth/signin",
  // signOut: '/auth/signout',
  // error: '/auth/error', // Error code passed in query string as ?error=
  // verifyRequest: '/auth/verify-request', // (used for check email message)
  // newUser: '/auth/new-user' // New users will be directed here on first sign in (leave the property out if not of interest)
};

export const authOptions: NextAuthOptions = {
  debug: true,
  secret: env.NEXTAUTH_SECRET,
  pages,
  providers,
  session: {
    // Choose how you want to save the user session.
    // The default is `"jwt"`, an encrypted JWT (JWE) stored in the session cookie.
    // If you use an `adapter` however, we default it to `"database"` instead.
    // You can still force a JWT session by explicitly defining `"jwt"`.
    // When using `"database"`, the session cookie will only contain a `sessionToken` value,
    // which is used to look up the session in the database.
    strategy: "jwt",

    // Seconds - How long until an idle session expires and is no longer valid.
    maxAge: 15 * 60 * 60, // 15 minutes
  },
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        // This will only be executed at login. Each next invocation will skip this part.
        const data = user;
        token.name = data.user.name;
        token.email = data.user.email;
        token.user = data.user;
        token.accessToken = data.accessToken;
        token.refreshToken = data.refreshToken;
      }

      return token;
    },
    async session({ session, token }) {
      // Send properties to the client, like an access_token and user id from a provider.
      session.accessToken = token.accessToken;
      session.refreshToken = token.refreshToken;
      session.user = token.user;

      return Promise.resolve(session);
    },
  },
} satisfies NextAuthOptions;

// Use it in server contexts
export function auth(
  ...args: [GetServerSidePropsContext["req"], GetServerSidePropsContext["res"]] | [NextApiRequest, NextApiResponse] | []
) {
  return getServerSession(...args, authOptions);
}
