import React, { ReactNode } from "react";

export interface DataTableHooks {
  topRight?: ReactNode;
  topLeft?: ReactNode;
  topCenter?: ReactNode;
  bottomLeft?: ReactNode;
  bottomCenter?: ReactNode;
  bottomRight?: ReactNode;
}

export interface DataTableColumn {
  label: string;
  key?: string;
  orderKey?: string;
  accessor?: (row) => React.ReactNode | string;
}

export interface LogEntry {
  id: number;
  action: string;
  loggedAt: number;
  objectClass: string;
  objectId: string;
  username: string | null;
  version: number;
  data: object;
}
