export interface Errors {
  [fieldName: string]: string[];
}

export interface CustomResponse extends Response {
  errors?: Errors;
  message?: string;
  status_code?: number;
  data?: never;
}

export interface AuthResponse extends CustomResponse {
  data: {
    accessToken: string;
    refreshToken: string;
  };
}
