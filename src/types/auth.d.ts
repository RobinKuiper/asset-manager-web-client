export interface AuthUser {
  name: string;
  email: string;
  role: string;
  permissions: string[];
}

export interface ApiUser {
  id: number;
  name: string;
  email: string;
  role: ApiRole;
  permissions: ApiPermission[];
  createdAt: number;
  updatedAt: number;
}

export interface ApiRole {
  id: number;
  name: string;
  permissions: ApiPermission[];
  users: ApiUser[];
  createdAt: number;
  updatedAt: number;
}

export interface ApiPermission {
  id: number;
  name: string;
  description: string;
  roles: ApiRole[];
  users: ApiUser[];
  createdAt: number;
  updatedAt: number;
}
