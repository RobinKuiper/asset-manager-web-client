import { AuthUser } from "@/types/auth";

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: AuthUser;
    accessToken: string;
    refreshToken: string;
  }

  interface User {
    user: AuthUser;
    accessToken: string;
    refreshToken: string;
  }
}

declare module "next-auth/jwt" {
  /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
  interface JWT {
    exp: number;
    iat: number;
    user: AuthUser;
    accessToken: string;
    refreshToken: string;
  }
}
