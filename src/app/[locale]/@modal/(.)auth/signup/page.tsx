import Modal from "@/components/layout/Modal";
import RegisterForm from "@/components/auth/RegisterForm";

export default function Page() {
  return (
    <Modal>
      <RegisterForm />
    </Modal>
  );
}
