import LoginForm from "@/components/auth/LoginForm";
import Modal from "@/components/layout/Modal";

export default function Page() {
  return (
    <Modal>
      <LoginForm />
    </Modal>
  );
}
