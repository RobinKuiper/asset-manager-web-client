import Modal from "@/components/layout/Modal";

export default async function Page() {
  return (
    <Modal>
      <h1>Modal Page</h1>
      <p>This modal is also accessible as a normal page.</p>
      <p>Refresh now to get the normal page.</p>
    </Modal>
  );
}
