import Modal from "@/components/layout/Modal";
import EditUserForm from "@/app/[locale]/admin/users/[userId]/components/EditUserForm";
import { doApiCall } from "@/actions/doApiCall";

export default async function Page() {
  const response = await doApiCall({ path: `/roles`, tag: "roles" });
  const roles = response.data;

  return (
    <Modal>
      <EditUserForm roles={roles} />
    </Modal>
  );
}
