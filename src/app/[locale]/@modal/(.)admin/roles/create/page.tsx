import Modal from "@/components/layout/Modal";
import EditRoleForm from "@/app/[locale]/admin/roles/[roleId]/components/EditRoleForm";

export default function Page() {
  return (
    <Modal>
      <EditRoleForm />
    </Modal>
  );
}
