import Modal from "@/components/layout/Modal";
import EditPermissionForm from "@/app/[locale]/admin/permissions/[permissionId]/components/EditPermissionForm";

export default function Page() {
  return (
    <Modal>
      <EditPermissionForm />
    </Modal>
  );
}
