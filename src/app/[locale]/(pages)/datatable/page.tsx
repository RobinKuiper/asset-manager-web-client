import DemoTable from "@/components/data/DemoTable";
import { faker } from "@faker-js/faker";
import env from "@/env.mjs";
import { useTranslations } from "next-intl";

export default function Page() {
  const t = useTranslations("DataTable");

  const data = [
    {
      id: 1,
      name: "Robin Kuiper",
      site: "https://rkuiper.nl",
    },
    {
      id: 2,
      name: "Remnant2Tools",
      site: "https://remnant.rkuiper.nl",
    },
    {
      id: 3,
      name: "Google",
      site: "https://google.nl",
    },
  ];

  if (env.NODE_ENV === "development") {
    for (let i = 4; i < 100; i++) {
      const domain = faker.internet.domainName();
      data.push({
        id: i,
        name: domain,
        site: `https://${domain}`,
      });
    }
  }

  return (
    <DemoTable
      data={data}
      locale={{
        title: t("title"),
        id: t("id"),
        name: t("name"),
        site: t("site"),
      }}
    />
  );
}
