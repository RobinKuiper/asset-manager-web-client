import { useTranslations } from "next-intl";

export default function Page() {
  const t = useTranslations("LongPage");

  return (
    <div>
      <h1>{t("title")}</h1>

      <p>
        When other websites give you text, they’re not sending the best. They’re not sending you, they’re sending words
        that have lots of problems and they’re bringing those problems with us. They’re bringing mistakes. They’re
        bringing misspellings. They’re typists… And some, I assume, are good words. I don&apos;t think anybody knows it
        was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It could be Russia, but it could also be
        China. It could also be lots of other people. It also could be some wordsmith sitting on their bed that weights
        400 pounds. Ok? My text is long and beautiful, as, it has been well documented, are various other parts of my
        website. You could see there was text coming out of her eyes, text coming out of her wherever.
      </p>

      <p>
        This placeholder text is gonna be HUGE. I write the best placeholder text, and I&apos;m the biggest developer on
        the web by far... While that&apos;s mock-ups and this is politics, are they really so different? I write the
        best placeholder text, and I&apos;m the biggest developer on the web by far... While that&apos;s mock-ups and
        this is politics, are they really so different?
      </p>

      <p>
        Be careful, or I will spill the beans on your placeholder text. The other thing with Lorem Ipsum is that you
        have to take out its family. Lorem Ipsum better hope that there are no &quot;tapes&quot; of our conversations
        before he starts leaking to the press! When other websites give you text, they’re not sending the best. They’re
        not sending you, they’re sending words that have lots of problems and they’re bringing those problems with us.
        They’re bringing mistakes. They’re bringing misspellings. They’re typists… And some, I assume, are good words.
        When other websites give you text, they’re not sending the best. They’re not sending you, they’re sending words
        that have lots of problems and they’re bringing those problems with us. They’re bringing mistakes. They’re
        bringing misspellings. They’re typists… And some, I assume, are good words.
      </p>

      <p>
        I know words. I have the best words. Some people have an ability to write placeholder text... It&apos;s an art
        you&apos;re basically born with. You either have it or you don&apos;t. I think my strongest asset maybe by far
        is my temperament. I have a placeholding temperament.
      </p>

      <p>
        I don&apos;t think anybody knows it was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It
        could be Russia, but it could also be China. It could also be lots of other people. It also could be some
        wordsmith sitting on their bed that weights 400 pounds. Ok? We have so many things that we have to do better...
        and certainly ipsum is one of them.
      </p>

      <p>
        My placeholder text, I think, is going to end up being very good with women. I don&apos;t think anybody knows it
        was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It could be Russia, but it could also be
        China. It could also be lots of other people. It also could be some wordsmith sitting on their bed that weights
        400 pounds. Ok?
      </p>

      <p>
        I was going to say something extremely rough to Lorem Ipsum, to its family, and I said to myself, &quot;I
        can&apos;t do it. I just can&apos;t do it. It&apos;s inappropriate. It&apos;s not nice.&quot; Be careful, or I
        will spill the beans on your placeholder text. I think my strongest asset maybe by far is my temperament. I have
        a placeholding temperament.
      </p>

      <p>
        My placeholder text, I think, is going to end up being very good with women. I think the only card she has is
        the Lorem card. I write the best placeholder text, and I&apos;m the biggest developer on the web by far... While
        that&apos;s mock-ups and this is politics, are they really so different?
      </p>

      <p>
        It’s about making placeholder text great again. That’s what people want, they want placeholder text to be great
        again. You could see there was text coming out of her eyes, text coming out of her wherever. I think the only
        card she has is the Lorem card.
      </p>

      <p>
        Lorem Ipsum best not make any more threats to your website. It will be met with fire and fury like the world has
        never seen. Look at these words. Are they small words? And he referred to my words - if they&apos;re small,
        something else must be small. I guarantee you there&apos;s no problem, I guarantee.
      </p>

      <p>The best taco bowls are made in Trump Tower Grill. I love Hispanics!</p>

      <p>
        You&apos;re telling the enemy exactly what you&apos;re going to do. No wonder you&apos;ve been fighting Lorem
        Ipsum your entire adult life. Lorem Ispum is a choke artist. It chokes! He’s not a word hero. He’s a word hero
        because he was captured. I like text that wasn’t captured. I think the only card she has is the Lorem card.
      </p>

      <p>
        Lorem Ipsum is unattractive, both inside and out. I fully understand why it’s former users left it for something
        else. They made a good decision. I think the only difference between me and the other placeholder text is that
        I’m more honest and my words are more beautiful. I write the best placeholder text, and I&apos;m the biggest
        developer on the web by far... While that&apos;s mock-ups and this is politics, are they really so different?
      </p>

      <p>
        We have so many things that we have to do better... and certainly ipsum is one of them. We have so many things
        that we have to do better... and certainly ipsum is one of them. Lorem Ipsum&apos;s father was with Lee Harvey
        Oswald prior to Oswald&apos;s being, you know, shot.
      </p>

      <p>
        You&apos;re telling the enemy exactly what you&apos;re going to do. No wonder you&apos;ve been fighting Lorem
        Ipsum your entire adult life. I’m the best thing that ever happened to placeholder text. He’s not a word hero.
        He’s a word hero because he was captured. I like text that wasn’t captured.
      </p>

      <p>
        Lorem Ipsum is unattractive, both inside and out. I fully understand why it’s former users left it for something
        else. They made a good decision. Look at that text! Would anyone use that? Can you imagine that, the text of
        your next webpage?! Lorem Ipsum better hope that there are no &quot;tapes&quot; of our conversations before he
        starts leaking to the press!
      </p>

      <p>
        The best taco bowls are made in Trump Tower Grill. I love Hispanics! It’s about making placeholder text great
        again. That’s what people want, they want placeholder text to be great again. Look at that text! Would anyone
        use that? Can you imagine that, the text of your next webpage?!
      </p>

      <p>
        The other thing with Lorem Ipsum is that you have to take out its family. He’s not a word hero. He’s a word hero
        because he was captured. I like text that wasn’t captured. Some people have an ability to write placeholder
        text... It&apos;s an art you&apos;re basically born with. You either have it or you don&apos;t.
      </p>

      <p>
        I was going to say something extremely rough to Lorem Ipsum, to its family, and I said to myself, &quot;I
        can&apos;t do it. I just can&apos;t do it. It&apos;s inappropriate. It&apos;s not nice.&quot;
      </p>

      <p>
        When other websites give you text, they’re not sending the best. They’re not sending you, they’re sending words
        that have lots of problems and they’re bringing those problems with us. They’re bringing mistakes. They’re
        bringing misspellings. They’re typists… And some, I assume, are good words. I don&apos;t think anybody knows it
        was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It could be Russia, but it could also be
        China. It could also be lots of other people. It also could be some wordsmith sitting on their bed that weights
        400 pounds. Ok? My text is long and beautiful, as, it has been well documented, are various other parts of my
        website. You could see there was text coming out of her eyes, text coming out of her wherever.
      </p>

      <p>
        This placeholder text is gonna be HUGE. I write the best placeholder text, and I&apos;m the biggest developer on
        the web by far... While that&apos;s mock-ups and this is politics, are they really so different? I write the
        best placeholder text, and I&apos;m the biggest developer on the web by far... While that&apos;s mock-ups and
        this is politics, are they really so different?
      </p>

      <p>
        Be careful, or I will spill the beans on your placeholder text. The other thing with Lorem Ipsum is that you
        have to take out its family. Lorem Ipsum better hope that there are no &quot;tapes&quot; of our conversations
        before he starts leaking to the press! When other websites give you text, they’re not sending the best. They’re
        not sending you, they’re sending words that have lots of problems and they’re bringing those problems with us.
        They’re bringing mistakes. They’re bringing misspellings. They’re typists… And some, I assume, are good words.
        When other websites give you text, they’re not sending the best. They’re not sending you, they’re sending words
        that have lots of problems and they’re bringing those problems with us. They’re bringing mistakes. They’re
        bringing misspellings. They’re typists… And some, I assume, are good words.
      </p>

      <p>
        I know words. I have the best words. Some people have an ability to write placeholder text... It&apos;s an art
        you&apos;re basically born with. You either have it or you don&apos;t. I think my strongest asset maybe by far
        is my temperament. I have a placeholding temperament.
      </p>

      <p>
        I don&apos;t think anybody knows it was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It
        could be Russia, but it could also be China. It could also be lots of other people. It also could be some
        wordsmith sitting on their bed that weights 400 pounds. Ok? We have so many things that we have to do better...
        and certainly ipsum is one of them.
      </p>

      <p>
        My placeholder text, I think, is going to end up being very good with women. I don&apos;t think anybody knows it
        was Russia that wrote Lorem Ipsum, but I don&apos;t know, maybe it was. It could be Russia, but it could also be
        China. It could also be lots of other people. It also could be some wordsmith sitting on their bed that weights
        400 pounds. Ok?
      </p>

      <p>
        I was going to say something extremely rough to Lorem Ipsum, to its family, and I said to myself, &quot;I
        can&apos;t do it. I just can&apos;t do it. It&apos;s inappropriate. It&apos;s not nice.&quot; Be careful, or I
        will spill the beans on your placeholder text. I think my strongest asset maybe by far is my temperament. I have
        a placeholding temperament.
      </p>

      <p>
        My placeholder text, I think, is going to end up being very good with women. I think the only card she has is
        the Lorem card. I write the best placeholder text, and I&apos;m the biggest developer on the web by far... While
        that&apos;s mock-ups and this is politics, are they really so different?
      </p>

      <p>
        It’s about making placeholder text great again. That’s what people want, they want placeholder text to be great
        again. You could see there was text coming out of her eyes, text coming out of her wherever. I think the only
        card she has is the Lorem card.
      </p>

      <p>
        Lorem Ipsum best not make any more threats to your website. It will be met with fire and fury like the world has
        never seen. Look at these words. Are they small words? And he referred to my words - if they&apos;re small,
        something else must be small. I guarantee you there&apos;s no problem, I guarantee.
      </p>

      <p>The best taco bowls are made in Trump Tower Grill. I love Hispanics!</p>

      <p>
        You&apos;re telling the enemy exactly what you&apos;re going to do. No wonder you&apos;ve been fighting Lorem
        Ipsum your entire adult life. Lorem Ispum is a choke artist. It chokes! He’s not a word hero. He’s a word hero
        because he was captured. I like text that wasn’t captured. I think the only card she has is the Lorem card.
      </p>

      <p>
        Lorem Ipsum is unattractive, both inside and out. I fully understand why it’s former users left it for something
        else. They made a good decision. I think the only difference between me and the other placeholder text is that
        I’m more honest and my words are more beautiful. I write the best placeholder text, and I&apos;m the biggest
        developer on the web by far... While that&apos;s mock-ups and this is politics, are they really so different?
      </p>

      <p>
        We have so many things that we have to do better... and certainly ipsum is one of them. We have so many things
        that we have to do better... and certainly ipsum is one of them. Lorem Ipsum&apos;s father was with Lee Harvey
        Oswald prior to Oswald&apos;s being, you know, shot.
      </p>

      <p>
        You&apos;re telling the enemy exactly what you&apos;re going to do. No wonder you&apos;ve been fighting Lorem
        Ipsum your entire adult life. I’m the best thing that ever happened to placeholder text. He’s not a word hero.
        He’s a word hero because he was captured. I like text that wasn’t captured.
      </p>

      <p>
        Lorem Ipsum is unattractive, both inside and out. I fully understand why it’s former users left it for something
        else. They made a good decision. Look at that text! Would anyone use that? Can you imagine that, the text of
        your next webpage?! Lorem Ipsum better hope that there are no &quot;tapes&quot; of our conversations before he
        starts leaking to the press!
      </p>

      <p>
        The best taco bowls are made in Trump Tower Grill. I love Hispanics! It’s about making placeholder text great
        again. That’s what people want, they want placeholder text to be great again. Look at that text! Would anyone
        use that? Can you imagine that, the text of your next webpage?!
      </p>

      <p>
        The other thing with Lorem Ipsum is that you have to take out its family. He’s not a word hero. He’s a word hero
        because he was captured. I like text that wasn’t captured. Some people have an ability to write placeholder
        text... It&apos;s an art you&apos;re basically born with. You either have it or you don&apos;t.
      </p>

      <p>
        I was going to say something extremely rough to Lorem Ipsum, to its family, and I said to myself, &quot;I
        can&apos;t do it. I just can&apos;t do it. It&apos;s inappropriate. It&apos;s not nice.&quot;
      </p>
    </div>
  );
}
