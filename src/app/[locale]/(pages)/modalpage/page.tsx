export default async function Page() {
  return (
    <div>
      <h1>Modal Page</h1>
      <p>This page is also accessible as a modal.</p>
    </div>
  );
}
