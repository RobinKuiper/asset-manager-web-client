import Debug from "@/components/Debug";

interface Props {
  searchParams: URLSearchParams;
}

export default async function Page({ searchParams }: Props) {
  return (
    <div>
      <h1>Forbidden</h1>
      <p>You don&apos;t have the right permissions to access this page.</p>

      <Debug
        data={Object.entries(searchParams).map(([key, value]) => {
          return {
            key,
            value,
          };
        })}
      />
    </div>
  );
}
