import Sidebar from "@/components/layout/Sidebar";
import DefaultAdminSidebarNav from "@/components/layout/DefaultAdminSidebarNav";
import { auth } from "@/auth";

export default async function Page() {
  const user = (await auth())?.user;

  return (
    <Sidebar position="right">
      <div className="sidebar-content">
        <p style={{ paddingLeft: "20px" }}>Another sidebar!</p>
        {user && <DefaultAdminSidebarNav />}
      </div>
    </Sidebar>
  );
}
