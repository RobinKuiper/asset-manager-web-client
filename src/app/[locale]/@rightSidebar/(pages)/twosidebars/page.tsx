import Sidebar from "@/components/layout/Sidebar";

export default async function Page() {
  return (
    <Sidebar position="right">
      <div className="sidebar-content">
        <p style={{ padding: "20px" }}>This is a sidebar on the right side</p>
      </div>
    </Sidebar>
  );
}
