import "@/app/[locale]/page.scss";
import PcGuy from "@/images/pcguy.webp";
import Image from "next/image";
import { useTranslations } from "next-intl";

export default function Page() {
  const t = useTranslations("Homepage");
  return (
    <div className="homepage-container">
      <h1>{t("title")}</h1>
      <p>{t("subtitle")}</p>
      <div className="image">
        <Image
          src={PcGuy}
          alt="Boilerplate Pc Guy"
          fill
          style={{
            objectFit: "contain",
            maskImage: "radial-gradient(circle at 50% 50%, black 30%, transparent 40%)",
          }}
        />
      </div>
    </div>
  );
}
