import Sidebar from "@/components/layout/Sidebar";
import DefaultFrontendSidebarNav from "@/components/layout/DefaultFrontendSidebarNav";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <DefaultFrontendSidebarNav />
      </div>
    </Sidebar>
  );
}
