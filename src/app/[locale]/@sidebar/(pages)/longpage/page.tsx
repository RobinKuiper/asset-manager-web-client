import Sidebar from "@/components/layout/Sidebar";
import DefaultFrontendSidebarNav from "@/components/layout/DefaultFrontendSidebarNav";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <p style={{ padding: "10px" }}>We can have different sidebars per page.</p>
        <DefaultFrontendSidebarNav />
      </div>
    </Sidebar>
  );
}
