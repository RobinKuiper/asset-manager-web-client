import Sidebar from "@/components/layout/Sidebar";
import DefaultAdminSidebarNav from "@/components/layout/DefaultAdminSidebarNav";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <DefaultAdminSidebarNav />
      </div>
    </Sidebar>
  );
}
