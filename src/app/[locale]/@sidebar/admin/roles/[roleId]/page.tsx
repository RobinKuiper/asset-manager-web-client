import Sidebar from "@/components/layout/Sidebar";
import DefaultAdminSidebarNav from "@/components/layout/DefaultAdminSidebarNav";
import Link from "next/link";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <Link href={"/admin/roles/create"} className="button" title="Create role">
          Create role
        </Link>

        <DefaultAdminSidebarNav />
      </div>
    </Sidebar>
  );
}
