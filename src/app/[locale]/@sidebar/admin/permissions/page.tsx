import Sidebar from "@/components/layout/Sidebar";
import DefaultAdminSidebarNav from "@/components/layout/DefaultAdminSidebarNav";
import Link from "next/link";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <Link href={"/admin/permissions/create"} className="button" title="Create permission">
          Create permission
        </Link>

        <DefaultAdminSidebarNav />
      </div>
    </Sidebar>
  );
}
