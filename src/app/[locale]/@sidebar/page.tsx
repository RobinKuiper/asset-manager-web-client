import Sidebar from "@/components/layout/Sidebar";
import DefaultFrontendSidebarNav from "@/components/layout/DefaultFrontendSidebarNav";
import SidebarButton from "@/components/layout/SidebarButton";

export default async function Page() {
  return (
    <Sidebar>
      <div className="sidebar-content">
        <SidebarButton />

        <DefaultFrontendSidebarNav />
      </div>
    </Sidebar>
  );
}
