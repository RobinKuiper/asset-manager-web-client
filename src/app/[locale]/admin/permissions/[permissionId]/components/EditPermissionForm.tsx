"use client";

import { useState } from "react";
import { doApiCall } from "@/actions/doApiCall";
import { ApiPermission } from "@/types/auth";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { CircleLoader } from "react-spinners";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { PermissionValidation } from "@/validations/PermissionValidation";

interface Props {
  permission?: ApiPermission;
}

export default function EditPermissionForm({ permission }: Props) {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof PermissionValidation>>({
    resolver: zodResolver(PermissionValidation),
    defaultValues: permission,
  });
  const router = useRouter();
  const path = permission ? `/permissions/${permission.id}` : "/permissions";
  const method = permission ? "PATCH" : "POST";
  const revalidate = permission ? ["permission", "permission_logs"] : null;
  const [loading, setLoading] = useState(false);

  const handleEdit = handleSubmit(async data => {
    setLoading(true);

    const result = await doApiCall({ path, method, body: data, revalidate });

    if (!result || !result.ok) {
      toast.error("Something went wrong.");
      setLoading(false);
      return;
    }

    if (permission) {
      toast.success("Saved.");
    } else {
      router.back();

      setTimeout(() => {
        router.replace(`/admin/permissions/${result.data.id}`);
      }, 100);
    }

    setLoading(false);
  });

  return (
    <form onSubmit={handleEdit} className="edit-form">
      <div className="heading">
        <h2>{permission ? "Update Permission" : "Create Permission"}</h2>
      </div>

      <label className="form-field">
        <span>Name</span>
        <input id="name" type="name" autoComplete="off" required placeholder="Permission name" {...register("name")} />
        {errors.name?.message && <div className="error">{errors.name?.message}</div>}
      </label>

      <label className="form-field">
        <span>Description</span>
        <input
          id="description"
          type="description"
          autoComplete="off"
          required
          placeholder="Permission description"
          className={errors && errors["description"] ? "error" : ""}
          {...register("description")}
        />
        {errors.description?.message && <div className="error">{errors.description?.message}</div>}
      </label>

      <div className="buttons">
        <button type="submit" className="button">
          {loading ? <CircleLoader color="white" size={10} /> : permission ? "Update" : "Create"}
        </button>
      </div>
    </form>
  );
}
