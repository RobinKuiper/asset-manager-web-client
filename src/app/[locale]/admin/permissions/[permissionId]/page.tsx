import "@/styles/admin/defaultPage.scss";
import { doApiCall } from "@/actions/doApiCall";
import LogTable from "@/components/data/LogTable";
import EditPermissionForm from "@/app/[locale]/admin/permissions/[permissionId]/components/EditPermissionForm";
import UserTable from "@/components/data/UserTable";
import RolesTable from "@/components/data/RolesTable";
import { ApiPermission } from "@/types/auth";

interface Props {
  params: { permissionId: string };
}

export default async function Page({ params }: Props) {
  const permissionResponse = await doApiCall({ path: `/permissions/${params.permissionId}`, tag: "permission" });
  // const permissionsResponse = await doApiCall({ path: `/permissions`, tag: 'permissions' })
  const logResponse = await doApiCall({ path: `/permissions/${params.permissionId}/logs`, tag: "permission_logs" });
  const permission: ApiPermission = permissionResponse.data;
  // const permissions = permissionsResponse.data;
  const logs = logResponse.data;

  return (
    <div className="admin-page-container">
      <h1>{permission.name}</h1>

      <div className="admin-page-content">
        <EditPermissionForm permission={permission} />

        {/*<div className="right">*/}
        <UserTable users={permission.users} />
        <RolesTable roles={permission.roles} />
        <LogTable logs={logs} />
        {/*</div>*/}
      </div>
    </div>
  );
}
