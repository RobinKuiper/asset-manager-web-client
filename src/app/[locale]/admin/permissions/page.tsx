import PermissionTable from "@/components/data/PermissionTable";

export default async function Page() {
  return <PermissionTable showDeleteAction={true} />;
}
