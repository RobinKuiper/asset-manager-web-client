import EditPermissionForm from "@/app/[locale]/admin/permissions/[permissionId]/components/EditPermissionForm";

export default async function Page() {
  return (
    <div className="admin-page-container">
      <EditPermissionForm />
    </div>
  );
}
