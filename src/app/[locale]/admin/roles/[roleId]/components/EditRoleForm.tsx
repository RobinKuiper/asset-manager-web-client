"use client";

import { useState } from "react";
import { doApiCall } from "@/actions/doApiCall";
import { ApiRole } from "@/types/auth";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { CircleLoader } from "react-spinners";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { RoleValidation } from "@/validations/RoleValidation";

interface Props {
  role?: ApiRole;
}

export default function EditRoleForm({ role }: Props) {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof RoleValidation>>({
    resolver: zodResolver(RoleValidation),
    defaultValues: role,
  });
  const router = useRouter();
  const path = role ? `/roles/${role.id}` : "/roles";
  const method = role ? "PATCH" : "POST";
  const revalidate = role ? ["role", "role_logs"] : null;
  const [loading, setLoading] = useState(false);

  const handleEdit = handleSubmit(async data => {
    setLoading(true);

    const result = await doApiCall({ path, method, body: data, revalidate });

    if (!result || !result.ok) {
      toast.error("Something went wrong.");
      setLoading(false);
      return;
    }

    if (role) {
      toast.success("Saved.");
    } else {
      router.back();

      setTimeout(() => {
        router.replace(`/admin/roles/${result.data.id}`);
      }, 100);
    }

    setLoading(true);
  });

  return (
    <form onSubmit={handleEdit} className="edit-form">
      <div className="heading">
        <h2>{role ? "Update Role" : "Create Role"}</h2>
      </div>

      <label className="form-field">
        <span>Name</span>
        <input id="name" type="name" autoComplete="off" required placeholder="Role name" {...register("name")} />
        {errors.name?.message && <div className="error">{errors.name?.message}</div>}
      </label>

      <div className="buttons">
        <button type="submit" className="button">
          {loading ? <CircleLoader color="white" size={10} /> : role ? "Update" : "Create"}
        </button>
      </div>
    </form>
  );
}
