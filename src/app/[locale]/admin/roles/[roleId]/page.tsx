import "@/styles/admin/defaultPage.scss";
import { doApiCall } from "@/actions/doApiCall";
import PermissionTable from "@/components/data/PermissionTable";
import LogTable from "@/components/data/LogTable";
import EditRoleForm from "@/app/[locale]/admin/roles/[roleId]/components/EditRoleForm";
import UserTable from "@/components/data/UserTable";

interface Props {
  params: { roleId: string };
}

export default async function Page({ params }: Props) {
  const roleResponse = await doApiCall({ path: `/roles/${params.roleId}`, tag: "role" });
  // const permissionsResponse = await doApiCall({ path: `/permissions`, tag: 'permissions' })
  const logResponse = await doApiCall({ path: `/roles/${params.roleId}/logs`, tag: "role_logs" });
  const role = roleResponse.data;
  // const permissions = permissionsResponse.data;
  const logs = logResponse.data;

  return (
    <div className="admin-page-container">
      <h1>{role.name}</h1>

      <div className="admin-page-content">
        <EditRoleForm role={role} />
        <UserTable users={role.users} />
        <PermissionTable
          permissions={role.permissions}
          addPermission={{
            path: `/roles/${role.id}/permission`,
            revalidate: "role",
          }}
          unlinkOptions={{
            path: `/roles/${role.id}/permission`,
            revalidate: "role",
            successMsg: `Successfully removed permission`,
          }}
        />
        <LogTable logs={logs} />
      </div>
    </div>
  );
}
