import EditRoleForm from "@/app/[locale]/admin/roles/[roleId]/components/EditRoleForm";

export default async function Page() {
  return (
    <div className="admin-page-container">
      <EditRoleForm />
    </div>
  );
}
