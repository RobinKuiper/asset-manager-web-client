"use client";

import { useState } from "react";
import { doApiCall } from "@/actions/doApiCall";
import { ApiRole, ApiUser } from "@/types/auth";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { CircleLoader } from "react-spinners";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { UserValidation } from "@/validations/UserValidation";

interface Props {
  user?: ApiUser;
  roles: ApiRole[];
}

export default function EditUserForm({ user, roles }: Props) {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof UserValidation>>({
    resolver: zodResolver(UserValidation),
    defaultValues: user,
  });
  const router = useRouter();
  const path = user ? `/users/${user.id}` : "/users";
  const method = user ? "PATCH" : "POST";
  const revalidate = user ? ["user", "user_logs"] : null;
  const [loading, setLoading] = useState(false);

  const handleEdit = handleSubmit(async data => {
    setLoading(true);

    const result = await doApiCall({ path, method, body: data, revalidate });

    if (!result || !result.ok) {
      toast.error("Something went wrong.");
      setLoading(false);
      return;
    }

    if (user) {
      toast.success("Saved.");
    } else {
      router.back();

      setTimeout(() => {
        router.replace(`/admin/users/${result.data.id}`);
      }, 100);
    }

    setLoading(false);
  });

  return (
    <form onSubmit={handleEdit} className="edit-form">
      <div className="heading">
        <h2>{user ? "Update User" : "Create User"}</h2>
      </div>

      <label className="form-field">
        <span>Name</span>
        <input id="name" type="name" autoComplete="off" required placeholder="Your name" {...register("name")} />
        {errors.name?.message && <div className="error">{errors.name?.message}</div>}
      </label>

      <label className="form-field">
        <span>Email</span>
        <input
          id="email"
          type="email"
          autoComplete="off"
          required
          placeholder="Your email address"
          {...register("email")}
        />
        {errors.email?.message && <div className="error">{errors.email?.message}</div>}
      </label>

      <label className="form-field">
        <span>Role</span>
        <select id="role" {...register("roleId")}>
          {roles.map(role => (
            <option key={role.id} value={role.id}>
              {role.name}
            </option>
          ))}
        </select>
        {errors.roleId?.message && <div className="error">{errors.roleId?.message}</div>}
      </label>

      <div className="buttons">
        <button type="submit" className="button">
          {loading ? <CircleLoader color="white" size={10} /> : user ? "Update" : "Create"}
        </button>
      </div>
    </form>
  );
}
