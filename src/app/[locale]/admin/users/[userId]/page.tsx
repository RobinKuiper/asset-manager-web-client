import "@/styles/admin/defaultPage.scss";
import { doApiCall } from "@/actions/doApiCall";
import EditUserForm from "@/app/[locale]/admin/users/[userId]/components/EditUserForm";
import PermissionTable from "@/components/data/PermissionTable";
import LogTable from "@/components/data/LogTable";

interface Props {
  params: { userId: string };
}

export default async function Page({ params }: Props) {
  const userResponse = await doApiCall({ path: `/users/${params.userId}`, tag: "user" });
  const rolesResponse = await doApiCall({ path: `/roles`, tag: "roles" });
  const logResponse = await doApiCall({ path: `/users/${params.userId}/logs`, tag: "user_logs" });
  const user = userResponse.data;
  const roles = rolesResponse.data;
  const logs = logResponse.data;

  return (
    <div className="admin-page-container">
      <h1>{user.name}</h1>

      <div className="admin-page-content">
        <EditUserForm user={user} roles={roles} />
        <PermissionTable
          permissions={user.permissions}
          addPermission={{
            path: `/users/${user.id}/permission`,
            revalidate: "user",
          }}
          unlinkOptions={{
            path: `/users/${user.id}/permission`,
            revalidate: "user",
            successMsg: `Successfully removed permission`,
          }}
        />
        <LogTable logs={logs} />
      </div>
    </div>
  );
}
