import EditUserForm from "@/app/[locale]/admin/users/[userId]/components/EditUserForm";
import { doApiCall } from "@/actions/doApiCall";

export default async function Page() {
  const response = await doApiCall({ path: `/roles`, tag: "roles" });
  const roles = response.data;

  return (
    <div className="admin-page-container">
      <EditUserForm roles={roles} />
    </div>
  );
}
