import "@/app/[locale]/auth/signup/registerPage.scss";
import RegisterForm from "@/components/auth/RegisterForm";

export default function Page() {
  return (
    <main className="registerpage">
      <div className="registerpage-content">
        <RegisterForm />
      </div>
    </main>
  );
}
