import "@/app/[locale]/auth/signin/loginPage.scss";
import LoginForm from "@/components/auth/LoginForm";

export default function Page() {
  return (
    <main className="loginpage">
      <div className="loginpage-content">
        <LoginForm />
      </div>
    </main>
  );
}
