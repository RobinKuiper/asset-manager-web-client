import { NextResponse, type NextMiddleware, type NextRequest } from "next/server";
import createMiddleware from "next-intl/middleware";
import { encode, getToken, type JWT } from "next-auth/jwt";
import { decodeJwtToken } from "@/utils/jwt";
import { getApiUrl } from "@/api";
import env from "./env.mjs";
import { AppConfig } from "@/AppConfig";

const SIGNIN_SUB_URL = "/api/auth/signin";
const SESSION_TIMEOUT = 60 * 60 * 24; // 24 hour
const TOKEN_REFRESH_BUFFER_SECONDS = 60;
const SESSION_SECURE = env.NEXTAUTH_URL?.startsWith("https://");
const SESSION_COOKIE = SESSION_SECURE ? "__Secure-next-auth.session-token" : "next-auth.session-token";
const FORBIDDEN_PATH = "/forbidden";

let isRefreshing = false;

interface RouteConfig {
  path: string;
  requiredRole?: string[];
  requiredPermissions?: string[];
}

const permissionRoutes: RouteConfig[] = [
  {
    path: "/admin/*",
    requiredRole: ["Admin"],
  },
  {
    path: "/protected",
    requiredPermissions: ["unknown_permission"],
  },
];

function shouldUpdateToken(token: JWT): boolean {
  const decoded: JWT = decodeJwtToken(token.accessToken);

  const timeInSeconds = Math.floor(Date.now() / 1000);
  return timeInSeconds >= decoded.exp - TOKEN_REFRESH_BUFFER_SECONDS;
}

async function refreshAccessToken(token: JWT) {
  if (isRefreshing) {
    return token;
  }

  console.info("Token refresh - Start", {
    email: token.email,
  });

  isRefreshing = true;

  const options: RequestInit = {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token.accessToken}`,
      "Refresh-Token": token.refreshToken,
    },
  };

  const response = await fetch(getApiUrl("/auth/token-refresh"), options).catch(error => {
    isRefreshing = false;
    console.error("Token refresh - Error", {
      email: token.email,
      error,
    });
    throw new Error(`Token refresh failed with status: ${error}`);
  });

  const responseData = await response.json();

  if (responseData.errors || !responseData.data.accessToken) {
    isRefreshing = false;
    console.error("Token refresh - Error", {
      email: token.email,
      errors: responseData.errors,
    });

    throw new Error(`Token refresh failed with status: ${response.status}`);
  }

  const data: JWT = decodeJwtToken(responseData.data.accessToken);

  console.info("Token refresh - Success", {
    email: token.email,
  });

  isRefreshing = false;
  return {
    ...token,
    ...responseData.data,
    iat: data.iat,
    exp: data.exp,
  };
}

function updateCookie(
  sessionToken: string | null,
  request: NextRequest,
  response: NextResponse,
): NextResponse<unknown> {
  if (sessionToken) {
    // Set the session token in the request and response cookies for a valid session
    request.cookies.set(SESSION_COOKIE, sessionToken);
    response = NextResponse.next({
      request: {
        headers: request.headers,
      },
    });
    response.cookies.set(SESSION_COOKIE, sessionToken, {
      httpOnly: true,
      maxAge: SESSION_TIMEOUT,
      secure: SESSION_SECURE,
      sameSite: "lax",
    });
  } else {
    // response.cookies.delete(SESSION_COOKIE);
    // request.cookies.delete(SESSION_COOKIE);

    // TODO: CHECK DELETING COOKIES
    const cookieValue = request.cookies
      .get(SESSION_COOKIE)
      ?.toString()
      .split(";")
      .find(c => c.trim().startsWith(`${SESSION_COOKIE}=`));
    response.headers.set("Set-Cookie", `${cookieValue}; Max-Age=0`);
    return NextResponse.redirect(new URL(SIGNIN_SUB_URL, request.url));
  }

  return response;
}

function matchRoute(request: NextRequest) {
  const pathName = request.nextUrl.pathname;
  return permissionRoutes.find(route => {
    const routePattern = route.path.replace(/\/\*/g, "(/.*)?");
    const regex = new RegExp(`^${routePattern}$`);
    return regex.test(pathName);
  });
}

function hasPermissions(request: NextRequest, token: JWT, config: RouteConfig) {
  if (config.requiredRole && config.requiredRole.indexOf(token.user.role) === -1) {
    return {
      type: "role",
      required_roles: config.requiredRole,
    };
  }

  if (config.requiredPermissions) {
    for (const permission of config.requiredPermissions) {
      if (token.user.permissions.indexOf(permission) === -1) {
        return {
          type: "permissions",
          missing_permission: permission,
          required_permissions: config.requiredPermissions,
        };
      }
    }
  }

  return true;
}

const intlMiddleware = createMiddleware({
  locales: AppConfig.locales,
  localePrefix: AppConfig.localePrefix,
  defaultLocale: AppConfig.defaultLocale,
});

export const middleware: NextMiddleware = async (request: NextRequest) => {
  let response = intlMiddleware(request);

  const pathName = request.nextUrl.pathname;
  const matchedRoute = matchRoute(request);
  if (!matchedRoute) {
    console.log(`Route '${pathName}' not matched`);
    return response;
  }

  console.log(`Route '${pathName}' matched`);

  const token = await getToken({ req: request });

  if (!token) {
    return Response.redirect(new URL(SIGNIN_SUB_URL, request.url));
  }

  if (shouldUpdateToken(token)) {
    if (!env.NEXTAUTH_SECRET) {
      throw new Error("NEXTAUTH_SECRET is not set.");
    }

    try {
      const newSessionToken = await encode({
        secret: env.NEXTAUTH_SECRET,
        token: await refreshAccessToken(token),
        maxAge: SESSION_TIMEOUT,
      });
      response = updateCookie(newSessionToken, request, intlMiddleware(request));
    } catch (error) {
      console.error("Token refresh - Error", {
        email: token.email,
        error,
      });
      return updateCookie(null, request, intlMiddleware(request));
    }
  }

  const rules = hasPermissions(request, token, matchedRoute);
  if (rules !== true) {
    let path = `${FORBIDDEN_PATH}?type=${rules.type}`;

    if (rules.type === "role") {
      path += `&required_roles=${rules.required_roles?.join("|")}`;
    } else if (rules.type === "permissions") {
      path += `&missing_permission=${rules.missing_permission}&required_permissions=${rules.required_permissions?.join("|")}`;
    }

    return Response.redirect(new URL(path, request.url));
  }

  return response;
};

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - _next/image (image optimization files)
     * - favicon.ico (favicon file)
     */
    "/((?!api|_next/static|_next/image|favicon.ico).*)",
  ],
};
