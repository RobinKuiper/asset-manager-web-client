"use server";

import { auth } from "@/auth";
import { getApiUrl } from "@/api";
import { logger } from "@/libs/logger";
import { revalidateTag } from "next/cache";

interface Params {
  path: string;
  method?: "POST" | "GET" | "PUT" | "PATCH" | "DELETE";
  body?: string | object | null;
  tag?: string | null;
  revalidate?: string | string[] | null;
}

export async function doApiCall({ path, method = "GET", body = null, tag = null, revalidate = null }: Params) {
  const session = await auth();

  logger.info(`doApiCall`, {
    method,
    body,
    path,
    user: session?.user.email,
  });

  const options: RequestInit = {
    mode: "cors",
    method,
    headers: {
      "Content-Type": "application/json",
      Authorization: session ? "Bearer " + session.accessToken : "",
    },
  };

  if (tag) {
    if (!options.next) {
      options.next = {};
    }
    options.next.tags = [tag];
  }

  if (body) {
    options.body = typeof body === "object" ? JSON.stringify(body) : body;
  }

  return fetch(getApiUrl(path), options)
    .then(async response => {
      if (!response.ok) {
        return Promise.reject(response.json());
      }

      if (revalidate) {
        if (Array.isArray(revalidate)) {
          revalidate.forEach(tag => {
            revalidateTag(tag);
          });
        } else {
          revalidateTag(revalidate);
        }
      }

      try {
        const text = await response.text();
        return JSON.parse(text);
      } catch (err) {
        return { ok: true };
      }
    })
    .then(data => {
      logger.debug("doApiCall - Success", {
        data,
        method,
        body,
        path,
        user: session?.user.email,
      });
      return data;
    })
    .catch(async error => {
      const err = await error;
      logger.error("doApiCall - Error", {
        error: {
          message: err.message,
        },
        method,
        body,
        path,
        user: session?.user.email,
      });
      console.log(err);
      return err;
    });
}
