import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Footer from "./Footer";

jest.mock("next/navigation", () => ({
  useRouter: jest.fn(() => ({
    replace: jest.fn(),
  })),
  usePathname: jest.fn(() => "/testpath"),
}));

test("renders footer with logo, navigation link, and locale switcher", () => {
  const currentLocale = "en";

  render(<Footer currentLocale={currentLocale} />);

  const logoImage = screen.getByRole("img", { name: "Robin Kuiper Logo" });
  const navigationLink = screen.getByRole("link", { name: "Gitlab" });
  const localeSwitcher = screen.getByRole("combobox");

  expect(logoImage).toBeInTheDocument();
  expect(navigationLink).toBeInTheDocument();
  expect(localeSwitcher).toBeInTheDocument();
});
