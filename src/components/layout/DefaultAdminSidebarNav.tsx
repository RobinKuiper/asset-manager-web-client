import Link from "next/link";
import { FaUnlockKeyhole, FaUserNurse, FaUsers } from "react-icons/fa6";
import { GoLog } from "react-icons/go";
import { useTranslations } from "next-intl";

export default function DefaultAdminSidebarNav() {
  const t = useTranslations();
  return (
    <nav>
      <Link href={"/admin/users"} className="icon-item">
        <FaUsers />
        {t("Global.users")}
      </Link>
      <Link href={"/admin/roles"} className="icon-item">
        <FaUserNurse />
        {t("Global.roles")}
      </Link>
      <Link href={"/admin/permissions"} className="icon-item">
        <FaUnlockKeyhole />
        {t("Global.permissions")}
      </Link>
      <Link href={"/admin/logs"} className="icon-item">
        <GoLog />
        {t("Global.logs")}
      </Link>
    </nav>
  );
}
