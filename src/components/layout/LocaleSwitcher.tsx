import { usePathname, useRouter } from "next/navigation";
import React from "react";

interface Props {
  currentLocale: string;
}

export default function LocaleSwitcher({ currentLocale }: Props) {
  const { replace } = useRouter();
  const pathname = usePathname();

  const localeTable: { [locale: string]: { [locale: string]: string } } = {
    en: {
      en: "English",
      nl: "Dutch",
    },
    nl: {
      en: "Engels",
      nl: "Nederlands",
    },
    fr: {
      en: "Anglais",
      es: "Espagnol",
      fr: "Français",
      de: "Allemand",
    },
    de: {
      en: "Englisch",
      es: "Spanisch",
      fr: "Französisch",
      de: "Deutsch",
    },
  };
  const localeOptions = localeTable[currentLocale];

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newLocale = e.target.value;

    if (newLocale === currentLocale) return;

    const newPathname = pathname.replace(`/${currentLocale}`, "");

    replace(`/${newLocale}${newPathname}`);
  };

  return (
    <select defaultValue={currentLocale} onChange={handleChange}>
      {Object.entries(localeOptions).map(([locale, label]) => (
        <option key={locale} value={locale}>
          {label}
        </option>
      ))}
    </select>
  );
}
