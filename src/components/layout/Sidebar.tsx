"use client";

import "@/styles/layout/sidebar.scss";
import React, { useEffect, useRef, useState } from "react";
import { usePathname } from "next/navigation";
import { PiCaretLeftBold, PiCaretRightBold } from "react-icons/pi";

interface Props {
  children: React.ReactNode;
  position?: "left" | "right";
}

export default function Sidebar({ children, position = "left" }: Props) {
  const ref = useRef<HTMLDivElement>(null);
  const pathname = usePathname();
  const [isOpen, setIsOpen] = useState(false);
  const className = isOpen ? "active" : "";
  const openIcon = position === "left" ? <PiCaretLeftBold /> : <PiCaretRightBold />;
  const closeIcon = position === "left" ? <PiCaretRightBold /> : <PiCaretLeftBold />;

  useEffect(() => {
    setIsOpen(false);
  }, [pathname]);

  const toggleOpen = () => {
    setIsOpen(!isOpen);

    const handlers: NodeListOf<HTMLDivElement> = document.querySelectorAll(".handler");
    handlers.forEach(handler => {
      if (handler !== ref.current) {
        handler.style.opacity = isOpen ? "1" : "0";
      }
    });
  };

  return (
    <>
      <div ref={ref} className={`handler ${className} ${position}`}>
        <button type="button" onClick={toggleOpen}>
          {isOpen ? openIcon : closeIcon}
        </button>
      </div>

      <div className={`sidebar ${className} ${position}`}>{children}</div>
    </>
  );
}
