import { fireEvent, render, screen } from "@testing-library/react";
import LocaleSwitcher from "./LocaleSwitcher";
import "@testing-library/jest-dom";
import { useRouter } from "next/navigation";

jest.mock("next/navigation", () => ({
  useRouter: jest.fn(() => ({
    replace: jest.fn(),
  })),
  usePathname: jest.fn(() => "/testpath"),
}));

test("renders locale switcher with options", () => {
  const currentLocale = "en";

  render(<LocaleSwitcher currentLocale={currentLocale} />);

  const select = screen.getByRole("combobox");
  const options: HTMLOptionElement[] = screen.getAllByRole("option");

  expect(select).toBeInTheDocument();
  expect(options.length).toBe(2);
  expect(options[0].selected).toBe(true);
  expect(options[0].textContent).toBe("English");
  expect(options[1].textContent).toBe("Dutch");
});

test("changes locale on select change", async () => {
  const currentLocale = "en";
  const mockReplace = jest.fn();
  (useRouter as jest.Mock).mockReturnValueOnce({ replace: mockReplace });

  render(<LocaleSwitcher currentLocale={currentLocale} />);

  const select: HTMLSelectElement = screen.getByRole("combobox");
  fireEvent.change(select, { target: { value: "nl" } });

  expect(mockReplace).toHaveBeenCalledWith("/nl/testpath");
});

test("does not change locale when selected option is the same", () => {
  const currentLocale = "en";
  const mockReplace = jest.fn();
  (useRouter as jest.Mock).mockReturnValueOnce({ replace: mockReplace });

  render(<LocaleSwitcher currentLocale={currentLocale} />);

  const select = screen.getByRole("combobox");
  fireEvent.change(select, { target: { value: "en" } });

  expect(mockReplace).not.toHaveBeenCalled();
});
