import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Topbar from "./Topbar";

const mockLocale = {
  protected: "Protected",
  signin: "Sign In",
  signout: "Sign Out",
};

const mockUser = { role: "User" };
const mockAdmin = { role: "Admin" };

test("renders topbar with logo, navigation links, and user menu (not signed in)", () => {
  render(<Topbar locale={mockLocale} />);

  const logoImage = screen.getByRole("img", { name: "Boilerplate Logo" });
  const navigationLinks = document.querySelectorAll("nav a");
  const userMenu = screen.getByRole("link", { name: mockLocale.signin });

  expect(logoImage).toBeInTheDocument();
  expect(navigationLinks.length).toBe(3);
  expect(userMenu).toBeInTheDocument();
});

test("renders navigation links with correct text and href", () => {
  render(<Topbar locale={mockLocale} />);

  const navigationLinks: NodeListOf<HTMLAnchorElement> = document.querySelectorAll("nav a");

  expect(navigationLinks[0].textContent).toBe("Home");
  expect(navigationLinks[0].href).toBe("http://localhost/");
  expect(navigationLinks[1].textContent).toBe(mockLocale.protected);
});

test("renders user menu with sign in link when not signed in", () => {
  render(<Topbar locale={mockLocale} />);

  const userMenu: HTMLAnchorElement = screen.getByRole("link", { name: mockLocale.signin });

  expect(userMenu.textContent).toBe(mockLocale.signin);
  expect(userMenu.href).toContain("/auth/signin");
});

test("renders user menu with sign out button when signed in", () => {
  render(<Topbar user={mockUser} locale={mockLocale} />);

  const signOutButton = screen.getByRole("button", { name: mockLocale.signout });

  expect(signOutButton.textContent).toBe(mockLocale.signout);
});

test("renders admin link only for users with admin role", () => {
  render(<Topbar user={mockUser} locale={mockLocale} />); // No admin user

  expect(screen.queryByRole("link", { name: "Admin" })).not.toBeInTheDocument();

  render(<Topbar user={mockAdmin} locale={mockLocale} />);

  const adminLink = screen.getByRole("link", { name: "Admin" });

  expect(adminLink).toBeInTheDocument();
});
