"use client";

import "@/styles/modal.scss";
import React, { useEffect } from "react";
import { useRouter } from "next/navigation";

interface Props {
  children: React.ReactNode;
  isOpen?: boolean;
  close?: () => void;
  selector?: string;
}

export default function Modal({ children, isOpen, close, selector = "body" }: Props) {
  const clientModal = typeof isOpen === "boolean";
  const router = useRouter();

  useEffect(() => {
    if (!clientModal) return;

    const body = document.querySelector(selector),
      modal = document.querySelector(".modal"),
      overlay = document.querySelector(".modal-overlay");

    if (!body || !modal || !overlay) return;

    if (isOpen) {
      body.appendChild(modal);
      body.appendChild(overlay);
    }
  }, [isOpen, selector, clientModal]);

  if (clientModal && !isOpen) {
    return "";
  }

  const handleClose = () => {
    console.log("Handle close");
    if (clientModal && close) {
      const parent = document.querySelector(".modal-parent"),
        modal = document.querySelector(".modal"),
        overlay = document.querySelector(".modal-overlay");

      if (parent && modal && overlay) {
        parent.appendChild(modal);
        parent.appendChild(overlay);
      }

      close();
    } else {
      router.back();
    }
  };

  return (
    <div className="modal-parent" style={clientModal ? { display: "none" } : {}}>
      <div className="modal">{children}</div>

      <div className="modal-overlay" onClick={handleClose} />
    </div>
  );
}
