"use client";

import "@/styles/layout/topbar.scss";
import Link from "next/link";
import Image from "next/image";
import Logo from "@/images/logo.webp";
import HamburgerButton from "@/components/layout/HamburgerButton";
import { AuthUser } from "@/types/auth";
import { usePathname } from "next/navigation";
import { signOut } from "next-auth/react";

interface Props {
  locale: { [key: string]: string };
  user?: AuthUser;
}

export default function Topbar({ user, locale }: Props) {
  const pathname = usePathname();

  return (
    <div className="topbar">
      <div className="topbar-head">
        <div className="logo">
          <Link className="image" href="/" title="Boilerplate - Homepage">
            <Image
              src={Logo}
              alt="Boilerplate Logo"
              fill
              style={{
                objectFit: "contain",
              }}
            />
          </Link>
        </div>

        <HamburgerButton />
      </div>

      <nav>
        <Link href={"/"}>Home</Link>
        <Link href={"/protected"}>{locale.protected}</Link>
        <Link href={"https://rkuiper.nl"} target="_blank">
          RKuiper
        </Link>
      </nav>

      <div className="right">
        {user?.role === "Admin" && (
          <Link href={"/admin"} className="button">
            Admin
          </Link>
        )}
        <div className="user-menu">
          {!user ? (
            <Link className={"button"} href={`/auth/signin?callbackUrl=${pathname}`} title="Sign in">
              {locale.signin}
            </Link>
          ) : (
            <button type="button" className={"button"} onClick={() => signOut()}>
              {locale.signout}
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
