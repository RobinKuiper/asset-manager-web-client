"use client";

import { AppProgressBar as ProgressBar } from "next-nprogress-bar";

export default function LoaderBar() {
  return <ProgressBar height="4px" color="#be7125" options={{ showSpinner: false }} shallowRouting />;
}
