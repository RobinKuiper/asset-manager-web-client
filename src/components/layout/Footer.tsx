"use client";

import "@/styles/layout/footer.scss";
import Image from "next/image";
import Logo from "@/images/rklogo.webp";
import Link from "next/link";
import LocaleSwitcher from "@/components/layout/LocaleSwitcher";

interface Props {
  currentLocale: string;
}

export default function Footer({ currentLocale }: Props) {
  return (
    <footer>
      <div className="logo">
        <Link className="image" href="https://rkuiper.nl" title="rkuiper.nl - Website Robin Kuiper" target="_blank">
          <Image
            src={Logo}
            alt="Robin Kuiper Logo"
            fill
            style={{
              objectFit: "contain",
            }}
          />
        </Link>
      </div>

      <nav>
        <Link href={"https://gitlab.com/robinkuiper/"} title="Gitlab - Robin Kuiper" target="_blank">
          Gitlab
        </Link>
      </nav>

      <div>
        <LocaleSwitcher currentLocale={currentLocale} />
      </div>
    </footer>
  );
}
