"use client";

import { FaBars } from "react-icons/fa";
import { useEffect, useState } from "react";
import { usePathname } from "next/navigation";

export default function HamburgerButton() {
  const pathname = usePathname();
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(false);
  }, [pathname]);

  return (
    <div className={`hamburger ${isOpen ? "open" : ""}`} onClick={() => setIsOpen(!isOpen)}>
      <FaBars />
    </div>
  );
}
