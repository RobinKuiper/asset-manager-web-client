import Link from "next/link";
import {
  MdOutlineFitScreen,
  MdOutlinePanoramaWideAngle,
  MdOutlineReceiptLong,
  MdOutlineWidthWide,
} from "react-icons/md";
import { PiTableBold } from "react-icons/pi";
import { BsFillShieldLockFill } from "react-icons/bs";
import { useTranslations } from "next-intl";

export default function DefaultFrontendSidebarNav() {
  const t = useTranslations("Navigation");
  return (
    <nav>
      <Link href={"/longpage"} className="icon-item">
        <MdOutlineReceiptLong />
        {t("longpage")}
      </Link>
      <Link href={"/datatable"} className="icon-item">
        <PiTableBold />
        {t("datatable")}
      </Link>
      <Link href={"/modalpage"} className="icon-item">
        <MdOutlineFitScreen />
        {t("modalpage")}
      </Link>
      <Link href={"/protected"} className="icon-item">
        <BsFillShieldLockFill />
        {t("protected")}
      </Link>
      <Link href={"/twosidebars"} className="icon-item">
        <MdOutlineWidthWide />
        {t("twosidebars")}
      </Link>
      <Link href={"/nosidebar"} className="icon-item">
        <MdOutlinePanoramaWideAngle />
        {t("nosidebars")}
      </Link>
    </nav>
  );
}
