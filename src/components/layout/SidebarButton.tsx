"use client";

import Modal from "@/components/layout/Modal";
import { useState } from "react";

export default function SidebarButton() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <button type="button" className="button" title="Sidebar Button" onClick={() => setIsOpen(true)}>
        Sidebar Button
      </button>

      <Modal isOpen={isOpen} close={() => setIsOpen(false)}>
        <h1>Client modal</h1>
        <p>This opens a modal that is not also a page.</p>
      </Modal>
    </>
  );
}
