import "@/styles/debug.scss";
import { auth } from "@/auth";
import env from "@/env.mjs";

interface Props {
  data?: { key: string; value: string }[];
}

export default async function Debug({ data = [] }: Props) {
  const session = await auth();
  const user = session?.user;

  if (env.NODE_ENV !== "development") {
    return "";
  }

  return (
    <div className="debugging">
      <h2>Debugging</h2>
      <sup>Only visible in development mode</sup>

      {data.length > 0 && (
        <>
          <h3>Data</h3>
          <ul>
            {data.map(row => (
              <li key={row.key}>
                <b>{row.key}:</b> {row.value}
              </li>
            ))}
          </ul>
        </>
      )}

      {user && (
        <>
          <h3>User</h3>
          <ul>
            <li>
              <b>Name:</b> {user.name}
            </li>
            <li>
              <b>Email:</b> {user.email}
            </li>
            <li>
              <b>Role:</b> {user.role}
            </li>
            <li>
              <b>Permissions:</b> {user.permissions.length <= 0 ? "None" : user.permissions.join(",")}
            </li>
          </ul>
        </>
      )}

      {session && (
        <>
          <h3>Session</h3>
          <ul>
            <li>
              <b>Access token:</b> {session.accessToken}
            </li>
            <li>
              <b>Refresh token:</b> {session.refreshToken}
            </li>
          </ul>
        </>
      )}
    </div>
  );
}
