import "@/styles/search.scss";

import React, { useRef } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";

interface Props {
  callback: (result: string) => void;
  placeholder?: string;
  defaultValue?: string;
}

export default function Search({ callback, defaultValue = "", placeholder = "Search..." }: Props) {
  const ref = useRef<HTMLInputElement>(null);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    callback(e.target.value);
  };

  const handleClear = () => {
    if (!ref.current) return;
    // setQuery('');
    ref.current.value = "";
    callback("");
  };

  return (
    <div className="search-container">
      <input
        ref={ref}
        type="text"
        placeholder={placeholder}
        defaultValue={defaultValue}
        onChange={handleChange}
        autoFocus={true}
      />
      <span>
        {ref.current?.value !== "" ? (
          <button onClick={handleClear} type="button">
            <IoMdClose />
          </button>
        ) : (
          <AiOutlineSearch />
        )}
      </span>
    </div>
  );
}
