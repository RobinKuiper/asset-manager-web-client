"use client";

import DataTable from "@/components/data/DataTable";
import { DataTableColumn } from "@/types/data";
import Link from "next/link";
import DataTableActions from "@/components/data/DataTableActions";
import { ApiUser } from "@/types/auth";

interface Props {
  users?: ApiUser[];
}

export default function UserTable({ users }: Props) {
  const columns: DataTableColumn[] = [
    {
      label: "ID",
      key: "id",
      orderKey: "id",
    },
    {
      label: "Name",
      accessor: (row: ApiUser) => <Link href={`/admin/users/${row.id}`}>{row.name}</Link>,
      orderKey: "name",
    },
    {
      label: "Email",
      key: "email",
      orderKey: "email",
    },
    {
      label: "Role",
      key: "role.name",
    },
    {
      label: "",
      accessor: (row: ApiUser) => (
        <DataTableActions
          editPath={`/admin/users/${row.id}`}
          deleteOptions={{
            path: `/users/${row.id}`,
            revalidate: "users",
            successMsg: `Successfully deleted user '${row.name}'`,
          }}
        />
      ),
    },
  ];

  if (users) {
    return <DataTable title="Users" columns={columns} dataSet={users} />;
  }

  return <DataTable title="Users" columns={columns} url={"/users/paginated"} />;
}
