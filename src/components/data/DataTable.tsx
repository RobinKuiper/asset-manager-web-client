"use client";

import "@/styles/data/dataTable.scss";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Pagination from "@/components/data/Pagination";
import { DataTableColumn, DataTableHooks } from "@/types/data";
import { nanoid } from "nanoid";
import { getFieldValue } from "@/utils/objects";
import React, { useEffect, useState } from "react";
import Search from "@/components/Search";
import { doApiCall } from "@/actions/doApiCall";
import { FadeLoader } from "react-spinners";
import { useDebouncedCallback } from "use-debounce";

interface DataTableProps<T> {
  title: string;
  columns: DataTableColumn[];
  dataSet?: T[];
  url?: string;
  hooks?: DataTableHooks;
}

export default function DataTable<T>({ title, columns, dataSet, url, hooks }: DataTableProps<T>) {
  const [data, setData] = useState<T[]>([]);
  const [loading, setLoading] = useState(false);
  const [totalRecords, setTotalRecords] = useState<number>(dataSet?.length ?? 0);
  const [options, setOptions] = useState({
    start: 0,
    length: 10,
    orderBy: "",
    orderDir: "asc",
    query: "",
  });

  useEffect(() => {
    async function retrieveData() {
      setLoading(true);
      const params = Object.entries(options)
        .map(([key, value]) => `${key}=${value}`)
        .join("&");

      const result = await doApiCall({ path: `${url}?${params}`, tag: title.toLowerCase() });

      if (result.ok) {
        setData(result.data.data);
        setTotalRecords(result.data.recordsTotal);
      }
      setLoading(false);
    }

    function filterData() {
      if (!dataSet) return;

      // Filter data by query
      const filteredData = dataSet.filter(item =>
        Object.values(item as { [key: string]: unknown }).some(
          value => typeof value === "string" && value.toLowerCase().includes(options.query.toLowerCase()),
        ),
      ) as { [key: string]: unknown }[];

      // Sort data
      if (options.orderBy) {
        filteredData.sort((a, b) => {
          const aOrberBy = a[options.orderBy] as string;
          const bOrberBy = b[options.orderBy] as string;

          const order = options.orderDir === "asc" ? 1 : -1;
          if (aOrberBy < bOrberBy) return -1 * order;
          if (aOrberBy > bOrberBy) return 1 * order;
          return 0;
        });
      }

      // Paginate data
      setData(filteredData.slice(options.start, options.start + options.length) as T[]);
    }

    if (url) {
      retrieveData();
    } else if (dataSet) {
      filterData();
    }
  }, [title, url, dataSet, options]);

  const handleOrderChange = (key: string) => {
    let orderDir = options.orderDir;
    if (options.orderBy === key) {
      orderDir = options.orderDir === "asc" ? "desc" : "asc";
    }

    setOptions({
      ...options,
      orderBy: key,
      orderDir,
    });
  };

  const handlePerPageChange = (perPage: number) => {
    setOptions({
      ...options,
      length: perPage,
    });
  };

  const handlePageChange = (page: number) => {
    setOptions({
      ...options,
      start: options.length * page,
    });
  };

  const handleQueryChange = useDebouncedCallback((input: string) => {
    setOptions({
      ...options,
      query: input,
    });
  }, 300);

  return (
    <div className="datatable-container">
      <div className="head">
        {!hooks?.topLeft ? <h2 className="title">{title}</h2> : hooks.topLeft}

        {!hooks?.topCenter ? (
          <Pagination
            totalRecords={totalRecords}
            currentPage={Math.ceil(options.start / options.length + 1)}
            perPage={options.length}
            handlePageChange={handlePageChange}
            handlePerPageChange={handlePerPageChange}
          />
        ) : (
          hooks.topCenter
        )}

        {!hooks?.topRight ? <Search callback={handleQueryChange} defaultValue={options.query} /> : hooks.topRight}
      </div>

      <table>
        <thead>
          <tr key={nanoid()}>
            {columns.map(column => (
              <th
                key={nanoid()}
                className={`${column.orderKey ? "sortable" : ""} ${options.orderBy === column.orderKey ? "active" : ""}`}
                onClick={column.orderKey ? () => handleOrderChange(String(column.orderKey)) : undefined}
              >
                <div>
                  {column.label}
                  <span className={"sorter"}>
                    {options.orderDir === "desc" ? <TiArrowSortedDown /> : <TiArrowSortedUp />}
                  </span>
                </div>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {loading && (
            <div className="table-loader">
              <FadeLoader color="white" />
            </div>
          )}

          {data.length ? (
            data.map(row => (
              <tr key={nanoid()}>
                {columns.map(column => (
                  <td key={nanoid()} data-label={column.label}>
                    {column.accessor
                      ? column.accessor(row)
                      : (getFieldValue(row as { [key: string]: unknown }, column.key ?? "") as string)}
                  </td>
                ))}
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={columns.length}>No data found.</td>
            </tr>
          )}
        </tbody>
      </table>

      {!hooks?.bottomLeft ? <></> : hooks.bottomLeft}
      {!hooks?.bottomCenter ? <></> : hooks.bottomCenter}

      {!hooks?.bottomRight ? (
        <Pagination
          totalRecords={totalRecords}
          currentPage={Math.ceil(options.start / options.length + 1)}
          perPage={options.length}
          handlePageChange={handlePageChange}
          handlePerPageChange={handlePerPageChange}
        />
      ) : (
        hooks.bottomRight
      )}
    </div>
  );
}
