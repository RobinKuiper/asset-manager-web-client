"use client";

import DataTable from "@/components/data/DataTable";
import DataTableActions from "@/components/data/DataTableActions";
import { ApiRole } from "@/types/auth";

interface Props {
  roles?: ApiRole[];
}

export default function RolesTable({ roles }: Props) {
  const columns = [
    {
      label: "ID",
      key: "id",
      orderKey: "id",
    },
    {
      label: "Name",
      key: "name",
      orderKey: "name",
    },
    {
      label: "",
      accessor: (row: ApiRole) => (
        <DataTableActions
          editPath={`/admin/roles/${row.id}`}
          deleteOptions={{
            path: `/roles/${row.id}`,
            revalidate: "roles",
            successMsg: `Successfully deleted role '${row.name}'`,
          }}
        />
      ),
    },
  ];

  if (roles) {
    return <DataTable title="Roles" columns={columns} dataSet={roles} />;
  }

  return <DataTable title="Roles" columns={columns} url={"/roles/paginated"} />;
}
