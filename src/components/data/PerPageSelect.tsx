import React from "react";

interface Props {
  perPage: number;
  handlePerPageChange: (perPage: number) => void;
}

export default function PerPageSelect({ perPage, handlePerPageChange }: Props) {
  return (
    <select
      onChange={e => handlePerPageChange(Number(e.target.value))}
      className="button"
      defaultValue={perPage}
      data-testid="per-page-select"
    >
      <option value={10}>10</option>
      <option value={25}>25</option>
      <option value={50}>50</option>
      <option value={100}>100</option>
    </select>
  );
}
