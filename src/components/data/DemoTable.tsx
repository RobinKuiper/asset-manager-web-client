"use client";

import DataTable from "@/components/data/DataTable";
import { DataTableColumn } from "@/types/data";
import Link from "next/link";

interface Props {
  data: unknown[];
  locale: { [key: string]: string };
}

export default function DemoTable({ data, locale }: Props) {
  const columns: DataTableColumn[] = [
    {
      label: locale.id,
      key: "id",
      orderKey: "id",
    },
    {
      label: locale.name,
      key: "name",
      orderKey: "name",
    },
    {
      label: locale.site,
      accessor: row => <Link href={row.site}>{row.site}</Link>,
    },
  ];

  return <DataTable title={locale.title} columns={columns} dataSet={data} />;
}
