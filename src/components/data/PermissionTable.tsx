"use client";

import DataTable from "@/components/data/DataTable";
import DataTableActions from "@/components/data/DataTableActions";
import Modal from "@/components/layout/Modal";
import React, { FormEvent, useEffect, useState } from "react";
import { doApiCall } from "@/actions/doApiCall";
import { ApiPermission } from "@/types/auth";
import { toast } from "react-toastify";
import { FaPlus } from "react-icons/fa";
import { DataTableHooks } from "@/types/data";

interface Props {
  permissions?: ApiPermission[];
  addPermission?: {
    path: string;
    revalidate?: string;
  };
  showDeleteAction?: boolean;
  unlinkOptions?: {
    path: string;
    successMsg?: string;
    revalidate?: string | string[];
  };
}

export default function PermissionTable({
  permissions,
  addPermission,
  unlinkOptions,
  showDeleteAction = false,
}: Props) {
  const [isOpen, setIsOpen] = useState(false);
  const [allPermissions, setAllPermissions] = useState<ApiPermission[]>([]);

  useEffect(() => {
    const fetchPermissions = async () => {
      const response = await doApiCall({ path: "/permissions" });

      if (response.ok) {
        setAllPermissions(response.data);
      }
    };

    if (addPermission) {
      fetchPermissions();
    }
  }, [addPermission]);

  const columns = [
    {
      label: "ID",
      key: "id",
      orderKey: "id",
    },
    {
      label: "Name",
      key: "name",
      orderKey: "name",
    },
    {
      label: "Description",
      key: "description",
      orderKey: "description",
    },
    {
      label: "",
      accessor: (row: ApiPermission) => (
        <DataTableActions
          editPath={`/admin/permissions/${row.id}`}
          deleteOptions={
            !showDeleteAction
              ? undefined
              : {
                  path: `/permissions/${row.id}`,
                  revalidate: "permissions",
                  successMsg: `Successfully deleted permission '${row.name}'`,
                }
          }
          unlinkOptions={
            !unlinkOptions
              ? undefined
              : {
                  ...unlinkOptions,
                  path: `${unlinkOptions.path}/${row.id}`,
                }
          }
        />
      ),
    },
  ];

  let hooks: DataTableHooks = {};
  if (addPermission) {
    hooks = {
      topRight: (
        <button type="button" className="icon button" onClick={() => setIsOpen(true)}>
          <FaPlus /> Add
        </button>
      ),
    };
  }

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    if (!addPermission) return;

    const formData = new FormData(e.currentTarget);
    const permissionId = formData.get("permissionId");

    const response = await doApiCall({
      path: addPermission.path,
      method: "POST",
      body: JSON.stringify({ permissionId }),
      revalidate: addPermission.revalidate,
    });

    if (response.ok) {
      toast.success("Permission added");
      setIsOpen(false);
      return;
    }

    toast.error("Something went wrong");
  };

  return (
    <>
      {permissions ? (
        <DataTable title="Permissions" columns={columns} dataSet={permissions} hooks={hooks} />
      ) : (
        <DataTable title="Permissions" columns={columns} url={"/permissions/paginated"} hooks={hooks} />
      )}
      <Modal isOpen={isOpen} close={() => setIsOpen(false)}>
        <h2>Add permission</h2>

        <form onSubmit={handleSubmit}>
          <select name="permissionId">
            {allPermissions.map(permission => (
              <option key={permission.id} value={permission.id}>
                {permission.name}
              </option>
            ))}
          </select>

          <button type="submit" className="button">
            Save
          </button>
        </form>
      </Modal>
    </>
  );
}
