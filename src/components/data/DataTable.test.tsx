import "@testing-library/jest-dom";
import { act, fireEvent, render, screen } from "@testing-library/react";
import DataTable from "./DataTable";
import { faker } from "@faker-js/faker";

jest.mock("nanoid", () => {
  return {
    nanoid: () => {},
  };
});

const columns = [
  { label: "Name", accessor: (row: { name: string; age: number }) => row.name },
  { label: "Age", key: "age", orderKey: "age" },
];
const data = [
  { name: "John Doe", age: 30 },
  { name: "Jane Doe", age: 25 },
];

const multiplePageData = [];
for (let i = 0; i < 10; i++) {
  multiplePageData.push({
    name: faker.person.fullName(),
    age: faker.number.int({ min: 1, max: 85 }),
  });
}
multiplePageData.push({ name: "second page", age: 30 });
multiplePageData.push({ name: "second page 2", age: 25 });

test("renders data table with correct elements", () => {
  render(<DataTable title="User List" columns={columns} dataSet={data} />);

  const table = screen.getByRole("table");
  const title = screen.getByText("User List");
  const headers = screen.getAllByRole("columnheader");
  const rows = screen.getAllByRole("row");

  expect(table).toBeInTheDocument();
  expect(title).toHaveTextContent("User List");
  expect(headers.length).toBe(2);
  expect(headers[0]).toHaveTextContent("Name");
  expect(headers[1]).toHaveTextContent("Age");
  expect(rows.length).toBe(3); // Header + data
  expect(rows[1]).toHaveTextContent("John Doe");
  expect(rows[2]).toHaveTextContent("Jane Doe");
});

test("sorts data when column headers are clicked", () => {
  render(<DataTable title="User List" columns={columns} dataSet={data} />);

  const ageHeader = screen.getByRole("columnheader", { name: "Age" });
  const beforeSort = screen.getAllByRole("row");
  expect(beforeSort[1]).toHaveTextContent("John Doe");
  expect(beforeSort[2]).toHaveTextContent("Jane Doe");

  act(() => {
    fireEvent.click(ageHeader);
  });

  const sortedRows = screen.getAllByRole("row");
  expect(sortedRows[1]).toHaveTextContent("Jane Doe");
  expect(sortedRows[2]).toHaveTextContent("John Doe");

  act(() => {
    fireEvent.click(ageHeader);
  });

  const afterSort = screen.getAllByRole("row");
  expect(afterSort[1]).toHaveTextContent("John Doe");
  expect(afterSort[2]).toHaveTextContent("Jane Doe");
});

test("changes page when pagination buttons are clicked", () => {
  render(<DataTable title="Products" columns={columns} dataSet={multiplePageData} />);

  const nextButton = screen.getAllByRole("button", { name: ">" })[0];
  act(() => {
    fireEvent.click(nextButton);
  });

  const rows = screen.getAllByRole("row");
  expect(rows[1]).toHaveTextContent("second page");
  expect(rows[2]).toHaveTextContent("second page 2");
});

test("filters data based on search query", async () => {
  render(<DataTable title="Products" columns={columns} dataSet={data} />);

  const searchInput = screen.getByPlaceholderText("Search...");
  act(() => {
    fireEvent.change(searchInput, { target: { value: "Jane" } });
  });

  // Wait because of use-debounce
  await new Promise(r => setTimeout(r, 500));

  const filteredRows = screen.getAllByRole("row");
  expect(filteredRows.length).toBe(2);
  expect(filteredRows[1]).toHaveTextContent("Jane Doe");
});

test("shows more rows when perPage is changed", () => {
  render(<DataTable title="Products" columns={columns} dataSet={multiplePageData} />);

  const select: HTMLSelectElement = screen.getAllByRole("combobox")[0] as HTMLSelectElement;
  act(() => {
    fireEvent.change(select, { target: { value: "25" } });
  });

  const rows = screen.getAllByRole("row");
  expect(rows.length).toBe(13);
});
