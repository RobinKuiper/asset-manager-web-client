import "@testing-library/jest-dom";
import Pagination from "./Pagination";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

test("renders pagination elements", () => {
  render(
    <Pagination
      totalRecords={100}
      currentPage={1}
      perPage={10}
      handlePageChange={jest.fn()}
      handlePerPageChange={jest.fn()}
    />,
  );

  const paginationContainer = screen.getByTestId("pagination");
  const perPageSelect = screen.getByTestId("per-page-select");
  const previousButton = screen.getByTestId("previous-page");
  const pageButtons = screen.getAllByRole("button", { name: /^\d+$/ });
  const nextButton = screen.getByTestId("next-page");

  expect(paginationContainer).toBeInTheDocument();
  expect(perPageSelect).toBeInTheDocument();
  expect(previousButton).toBeInTheDocument();
  expect(pageButtons.length).toBe(10);
  expect(nextButton).toBeInTheDocument();
});

test("calls handlePageChange when a page button is clicked", async () => {
  const handlePageChange = jest.fn();
  render(
    <Pagination
      totalRecords={100}
      currentPage={2}
      perPage={10}
      handlePageChange={handlePageChange}
      handlePerPageChange={jest.fn()}
    />,
  );

  const page3Button = screen.getByRole("button", { name: "3" });
  await userEvent.click(page3Button);

  expect(handlePageChange).toHaveBeenCalledWith(2);
});

test("calls handlePerPageChange when per page select changes", async () => {
  const handlePerPageChange = jest.fn();
  render(
    <Pagination
      totalRecords={100}
      currentPage={1}
      perPage={10}
      handlePageChange={jest.fn()}
      handlePerPageChange={handlePerPageChange}
    />,
  );

  const perPageSelect = screen.getByTestId("per-page-select");
  await userEvent.selectOptions(perPageSelect, "25");

  expect(handlePerPageChange).toHaveBeenCalledWith(25);
});

test("disables previous button when on first page", () => {
  render(
    <Pagination
      totalRecords={100}
      currentPage={1}
      perPage={10}
      handlePageChange={jest.fn()}
      handlePerPageChange={jest.fn()}
    />,
  );

  const previousButton = screen.getByTestId("previous-page");
  expect(previousButton).toBeDisabled();
});

test("disables next button when on last page", () => {
  render(
    <Pagination
      totalRecords={100}
      currentPage={10}
      perPage={10}
      handlePageChange={jest.fn()}
      handlePerPageChange={jest.fn()}
    />,
  );

  const nextButton = screen.getByTestId("next-page");
  expect(nextButton).toBeDisabled();
});
