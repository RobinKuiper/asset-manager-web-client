"use client";

import DataTable from "@/components/data/DataTable";
import { DataTableColumn, LogEntry } from "@/types/data";
import { formatDateWithLocale } from "@/utils/date";

interface Props {
  logs?: LogEntry[];
}

export default function LogTable({ logs }: Props) {
  const columns: DataTableColumn[] = [
    {
      label: "ID",
      key: "id",
      orderKey: "id",
    },
    {
      label: "Entity",
      accessor: (row: LogEntry) => row.objectClass.split("\\").pop(),
    },
    {
      label: "Object ID",
      key: "objectId",
      orderKey: "objectId",
    },
    {
      label: "Action",
      key: "action",
      orderKey: "action",
    },
    {
      label: "By",
      key: "username",
      orderKey: "username",
    },
    {
      label: "Logged at",
      accessor: (row: LogEntry) => formatDateWithLocale(row.loggedAt),
      orderKey: "loggedAt",
    },
  ];

  if (logs) {
    return <DataTable title="Logs" columns={columns} dataSet={logs} />;
  }

  return <DataTable title="Logs" columns={columns} url={"/log-entries/paginated"} />;
}
