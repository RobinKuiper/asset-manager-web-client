"use client";

import { FaEdit, FaTrash, FaUnlink } from "react-icons/fa";
import Link from "next/link";
import { doApiCall } from "@/actions/doApiCall";
import { toast } from "react-toastify";

interface Props {
  editPath?: string;
  deleteOptions?: {
    path: string;
    successMsg?: string;
    revalidate?: string;
  };
  unlinkOptions?: {
    path: string;
    successMsg?: string;
    revalidate?: string | string[];
  };
}

export default function DataTableActions({ editPath, deleteOptions, unlinkOptions }: Props) {
  const showDeleteAction = deleteOptions && deleteOptions.path;
  const showUnlinkAction = unlinkOptions && unlinkOptions.path;

  const handleDelete = async () => {
    if (!showDeleteAction) return;

    const result = await doApiCall({
      path: deleteOptions.path,
      method: "DELETE",
      revalidate: deleteOptions?.revalidate,
    });

    if (!result.ok) {
      toast.error("Something went wrong.");
      return;
    }

    toast.success(deleteOptions?.successMsg ?? `Record deleted`);
  };

  const handleUnlink = async () => {
    if (!showUnlinkAction) return;

    const result = await doApiCall({
      path: unlinkOptions.path,
      method: "DELETE",
      revalidate: unlinkOptions?.revalidate,
    });

    if (!result.ok) {
      toast.error("Something went wrong.");
      return;
    }

    toast.success(unlinkOptions?.successMsg ?? `Record unlinked`);
  };

  return (
    <div className="actions">
      {editPath && (
        <Link href={editPath} title="Edit">
          <FaEdit />
        </Link>
      )}

      {showDeleteAction && (
        <button>
          <FaTrash onClick={handleDelete} />
        </button>
      )}
      {showUnlinkAction && (
        <button>
          <FaUnlink onClick={handleUnlink} />
        </button>
      )}
    </div>
  );
}
