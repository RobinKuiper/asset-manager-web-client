import "@/styles/data/pagination.scss";
import PerPageSelect from "@/components/data/PerPageSelect";

interface Props {
  totalRecords: number;
  currentPage: number;
  perPage: number;
  handlePageChange: (page: number) => void;
  handlePerPageChange: (perPage: number) => void;
}

export default function Pagination({
  totalRecords,
  perPage,
  currentPage,
  handlePageChange,
  handlePerPageChange,
}: Props) {
  const totalPages = Math.ceil(totalRecords / perPage);

  const pages = [];
  for (let i = 0; i < totalPages; i += 1) {
    pages.push(i);
  }

  return (
    <div className="pagination" data-testid="pagination">
      <PerPageSelect perPage={perPage} handlePerPageChange={handlePerPageChange} />

      <button
        onClick={() => handlePageChange(currentPage - 2)}
        className="button previous"
        disabled={currentPage - 2 < 0}
        data-testid="previous-page"
      >
        {"<"}
      </button>

      {pages.map(page => (
        <button
          key={page}
          onClick={() => handlePageChange(page)}
          className={`button page_${page} ${page + 1 === currentPage ? "active" : ""}`}
          disabled={page + 1 === currentPage}
        >
          {page + 1}
        </button>
      ))}

      <button
        onClick={() => handlePageChange(currentPage)}
        className="button next"
        disabled={currentPage + 1 > totalPages}
        data-testid="next-page"
      >
        {">"}
      </button>
    </div>
  );
}
