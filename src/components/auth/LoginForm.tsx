"use client";

import "@/styles/auth/loginForm.scss";
import { zodResolver } from "@hookform/resolvers/zod";
import Link from "next/link";
import { signIn } from "next-auth/react";
import { useState } from "react";
import { navigate } from "@/actions/navigate";
import { errors as errString } from "@/errors";
import { useSearchParams } from "next/navigation";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { LoginValidation } from "@/validations/AuthValidation";

export default function LoginForm() {
  const inputDefaults = {
    email: process.env.DEFAULT_EMAIL ?? "",
    password: process.env.DEFAULT_PASSWORD ?? "",
  };
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof LoginValidation>>({
    resolver: zodResolver(LoginValidation),
    defaultValues: process.env.NODE_ENV === "development" ? inputDefaults : undefined,
  });
  const params = useSearchParams();

  const [error, setError] = useState<string | null>(null);

  const handleLogin = handleSubmit(async data => {
    const result = await signIn("credentials", {
      email: data.email,
      password: data.password,
      redirect: false,
    });

    if (result?.ok) {
      await navigate(params.get("callbackUrl") ?? "/");
    }

    setError(result?.error ?? null);
  });

  return (
    <form onSubmit={handleLogin} className="login-form">
      <div className="heading">
        <h2>Sign In</h2>

        {error && (
          <p key={error} className="error">
            {errString[error]}
          </p>
        )}
      </div>

      <label className="form-field">
        <span>Email</span>
        <input
          id="email"
          type="email"
          autoComplete="off"
          required
          placeholder="Your email address"
          {...register("email")}
        />
        {errors.email?.message && <div className="error">{errors.email?.message}</div>}
      </label>

      <label className="form-field">
        <span>Password</span>
        <input
          id="password"
          type="password"
          autoComplete="off"
          required
          placeholder="Your password"
          {...register("password")}
        />
        {errors.password?.message && <div className="error">{errors.password?.message}</div>}
      </label>

      <div className="buttons">
        <button type="submit" className="button">
          Sign in
        </button>
        <Link href={"/auth/signup"} className="">
          Sign up
        </Link>
      </div>
    </form>
  );
}
