"use client";

import "@/styles/auth/registerForm.scss";
import Link from "next/link";
import { useState } from "react";
import { signIn } from "next-auth/react";
import { navigate } from "@/actions/navigate";
import { doApiCall } from "@/actions/doApiCall";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { RegisterValidation } from "@/validations/AuthValidation";
import { zodResolver } from "@hookform/resolvers/zod";

export default function RegisterForm() {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<z.infer<typeof RegisterValidation>>({
    resolver: zodResolver(RegisterValidation),
  });
  const [apiErrors, setApiErrors] = useState<{ [key: string]: string[] }>();

  const handleRegister = handleSubmit(async data => {
    const result = await doApiCall({ path: "/auth/register", method: "POST", body: data });

    if (!result || !result.ok) {
      setApiErrors(result.errors);
      return;
    }

    const signInResult = await signIn("credentials", {
      email: data.email,
      password: data.password,
      redirect: false,
    });

    if (signInResult?.ok) {
      await navigate("/");
    }
  });

  return (
    <form onSubmit={handleRegister} className="register-form">
      <div className="heading">
        <h2>Sign Up</h2>
      </div>

      <label className="form-field">
        <span>Name</span>
        <input id="name" type="name" autoComplete="off" required placeholder="Your name" {...register("name")} />
        {errors.name?.message && <div className="error">{errors.name?.message}</div>}
        {!apiErrors ||
          (apiErrors["name"] && (
            <div className="error">{apiErrors.name?.map(error => <div key={error}>{error}</div>)}</div>
          ))}
      </label>

      <label className="form-field">
        <span>Email</span>
        <input
          id="email"
          type="email"
          autoComplete="off"
          required
          placeholder="Your email address"
          {...register("email")}
        />
        {errors.email?.message && <div className="error">{errors.email?.message}</div>}
        {!apiErrors ||
          (apiErrors["email"] && (
            <div className="error">{apiErrors.email?.map(error => <div key={error}>{error}</div>)}</div>
          ))}
      </label>

      <label className="form-field">
        <span>Password</span>
        <input
          id="password"
          type="password"
          autoComplete="off"
          required
          placeholder="Your password"
          {...register("password")}
        />
        {errors.password?.message && <div className="error">{errors.password?.message}</div>}
        {!apiErrors ||
          (apiErrors["password"] && (
            <div className="error">{apiErrors.password?.map(error => <div key={error}>{error}</div>)}</div>
          ))}
      </label>

      <label className="form-field">
        <span>Password Confirmation</span>
        <input
          id="passwordConfirmation"
          type="password"
          autoComplete="off"
          required
          placeholder="Password Confirmation"
          {...register("confirmPassword")}
        />
        {errors.confirmPassword?.message && <div className="error">{errors.confirmPassword?.message}</div>}
        {!apiErrors ||
          (apiErrors["confirmPassword"] && (
            <div className="error">{apiErrors.confirmPassword?.map(error => <div key={error}>{error}</div>)}</div>
          ))}
      </label>

      <div className="buttons">
        <button type="submit" className="button">
          Sign Up
        </button>
        <Link href={"/auth/signin"} className="">
          Sign In
        </Link>
      </div>
    </form>
  );
}
