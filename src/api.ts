import env from "@/env.mjs";

interface Params {
  path: string;
  method?: "POST" | "GET" | "PUT" | "PATCH" | "DELETE";
  body?: string;
}

export function getApiUrl(path: string = ""): string {
  if (path.startsWith("http")) {
    return path;
  }

  const url = env.API_URL ?? "http://localhost:8000/api/";
  const version = env.API_VERSION ?? "v1";
  return url + version + path;
}

export default async function apiFetch({ path, method = "GET", body }: Params) {
  console.log("Api call");

  const options: RequestInit = {
    mode: "cors",
    method: method ?? "GET",
    // credentials: 'include', // Include cookies in the request
    headers: {
      "Content-Type": "application/json",
    },
  };

  // if (session) {
  //     options.headers['Authorization'] = "Bearer " + session.accessToken
  // }

  if (body) {
    options.body = body;
  }

  return fetch(getApiUrl(path), options)
    .then(response => {
      if (!response.ok) {
        return Promise.reject(response.json());
      }

      return response.json();
    })
    .then(data => {
      return data;
    })
    .catch(async error => {
      const err = await error;
      console.error("Request error:", err.message);
      return error;
    });
}
