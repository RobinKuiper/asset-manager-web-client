import { z } from "zod";

export const LoginValidation = z.object({
  email: z.string().email(),
  password: z.string().min(3),
});

export const RegisterValidation = z
  .object({
    name: z.string().min(3),
    email: z.string().email(),
    password: z.string().min(5),
    confirmPassword: z.string().min(5),
  })
  .refine(data => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });
