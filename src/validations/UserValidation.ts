import { z } from "zod";

export const UserValidation = z.object({
  name: z.string().min(3),
  email: z.string().email(),
  roleId: z.coerce.number(),
});
