import { z } from "zod";

export const RoleValidation = z.object({
  name: z.string().min(3),
});
