import type { LocalePrefix } from "node_modules/next-intl/dist/types/src/shared/types";

const localePrefix: LocalePrefix = "as-needed";

// FIXME: Set the correct values for your project
export const AppConfig = {
  locales: ["en", "nl"],
  defaultLocale: "en",
  localePrefix,
};
